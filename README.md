# README #

Android code for cLecture app. [cLecture](https://play.google.com/store/apps/details?id=fsousasilva.com.connectedlecture)

### Setup ###

* To run the project import the root folder as a Gradle project using Android Studio.
* Configure the PROD url within the the *app/build.gradle* file. 

### Server ###

You can deploy your own server. [cLecture server](https://bitbucket.org/fsousa/powerball_public)