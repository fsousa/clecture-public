package fsousasilva.com.connected_lecture.controller;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import fsousasilva.com.connected_lecture.model.User;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/23/16.
 */

public class UserController {

    private final String USEROBJECT = "photosharing_shared_user_object";
    private final String USERKEY = "photosharing_shared_user_key";

    public static UserController mInstance;

    private UserController() {
    }

    public static UserController getInstance() {
        if (mInstance == null) {
            mInstance = new UserController();
        }
        return mInstance;
    }

    public void saveUser(Activity ctx, User user) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(USERKEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new Gson();
        String auth = gson.toJson(user);
        editor.putString(USEROBJECT, auth);
        editor.apply();
    }

    public User getUser(Context ctx) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(USERKEY, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPref.getString(USEROBJECT, "");
        return gson.fromJson(json, User.class);
    }

    public void cleanUser(Context ctx) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(USERKEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(USEROBJECT);
        editor.apply();
    }
}
