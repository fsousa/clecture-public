package fsousasilva.com.connected_lecture.model;

import android.graphics.Bitmap;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/6/17.
 */

public class PDFDraw {

    private int height;
    private int width;
    private Bitmap image;
    private float offSetX;
    private float offSetY;
    private float zoom;


    public PDFDraw(int height, int width, Bitmap image, float offSetX, float offSetY, float zoom) {
        this.height = height;
        this.width = width;
        this.image = image;
        this.offSetX = offSetX;
        this.offSetY = offSetY;
        this.zoom = zoom;
    }


    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public float getOffSetX() {
        return offSetX;
    }

    public void setOffSetX(float offSetX) {
        this.offSetX = offSetX;
    }

    public float getOffSetY() {
        return offSetY;
    }

    public void setOffSetY(float offSetY) {
        this.offSetY = offSetY;
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }
}
