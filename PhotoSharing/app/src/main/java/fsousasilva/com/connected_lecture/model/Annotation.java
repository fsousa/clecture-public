package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/13/17.
 */

public class Annotation {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("draw")
    @Expose
    private Picture draw;
    @SerializedName("page")
    @Expose
    private int page;

    public Annotation(int id, String note, Picture draw, int page) {
        this.id = id;
        this.note = note;
        this.draw = draw;
        this.page = page;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Picture getDraw() {
        return draw;
    }

    public void setDraw(Picture draw) {
        this.draw = draw;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public static class List extends ArrayList<Annotation> {

    }
}
