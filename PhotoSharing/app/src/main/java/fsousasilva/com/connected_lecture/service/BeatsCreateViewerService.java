package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Status;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/22/16.
 */

public class BeatsCreateViewerService extends RetrofitSpiceRequest<Status, BeatsAPI> {

    private String token;
    private String name;

    public BeatsCreateViewerService( String token, String name) {
        super(Status.class, BeatsAPI.class);
        this.token = token;
        this.name = name;
    }

    @Override
    public Status loadDataFromNetwork() throws Exception {
        return getService().createViewer(getToken(), getName());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
