package fsousasilva.com.connected_lecture.model;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/5/17.
 */

public enum NotificationTypeEnum {

    GOTO ("goto"),
    RAISE_HAND("raise_hand"),
    ASK_PERMISSION_TO_DRAW("ask_permission_to_draw");


    private final  String name;

    NotificationTypeEnum(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
