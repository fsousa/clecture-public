package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/22/16.
 */

public class Presenter {

    @SerializedName("name")
    @Expose
    private String name;

    public Presenter(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static class List extends ArrayList<Presenter> {

    }
}
