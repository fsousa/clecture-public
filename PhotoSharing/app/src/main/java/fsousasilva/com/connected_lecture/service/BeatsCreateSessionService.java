package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Session;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/22/16.
 */

public class BeatsCreateSessionService extends RetrofitSpiceRequest<Session, BeatsAPI> {

    private String token;
    private String name;
    private String lat;
    private String lng;
    private Boolean isOpen;
    private String secret;

    public BeatsCreateSessionService(String token, String name, String lat, String lng, boolean isOpen, String secret) {
        super(Session.class, BeatsAPI.class);
        this.token = token;
        this.name = name.trim();
        this.isOpen = isOpen;
        this.lat = lat;
        this.lng = lng;
        this.secret = secret.trim();
    }

    @Override
    public Session loadDataFromNetwork() throws Exception {
        return getService().createSession(getToken(), getName(), getLat(), getLng(), getOpen(), getSecret());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
