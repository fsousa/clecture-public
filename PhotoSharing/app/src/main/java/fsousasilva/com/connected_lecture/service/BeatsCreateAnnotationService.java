package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Annotation;
import retrofit.mime.TypedFile;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/13/17.
 */

public class BeatsCreateAnnotationService extends RetrofitSpiceRequest<Annotation, BeatsAPI> {

    private String token;
    private String note;
    private int resourceId;
    private TypedFile draw;
    private int page;
    private String xOffset;
    private String yOffset;
    private String zoom;
    private String drawToken;


    public BeatsCreateAnnotationService(String token, String note, TypedFile draw, int resourceId, int page, String xOffset, String yOffset, String zoom) {
        super(Annotation.class, BeatsAPI.class);
        this.token = token;
        this.note = note;
        this.resourceId = resourceId;
        this.draw = draw;
        this.page = page;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.zoom = zoom;
    }

    public BeatsCreateAnnotationService(String token, String note, TypedFile draw, int resourceId, int page, String xOffset, String yOffset, String zoom, String drawToken) {
        super(Annotation.class, BeatsAPI.class);
        this.token = token;
        this.note = note;
        this.resourceId = resourceId;
        this.draw = draw;
        this.page = page;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.zoom = zoom;
        this.drawToken = drawToken;
    }

    @Override
    public Annotation loadDataFromNetwork() throws Exception {
        return getService().createAnnotation(getToken(), getNote(), getDraw(), getResourceId(), getPage(), getxOffset(), getyOffset(), getZoom(), getDrawToken());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public TypedFile getDraw() {
        return draw;
    }

    public void setDraw(TypedFile draw) {
        this.draw = draw;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getxOffset() {
        return xOffset;
    }

    public void setxOffset(String xOffset) {
        this.xOffset = xOffset;
    }

    public String getyOffset() {
        return yOffset;
    }

    public void setyOffset(String yOffset) {
        this.yOffset = yOffset;
    }

    public String getZoom() {
        return zoom;
    }

    public void setZoom(String zoom) {
        this.zoom = zoom;
    }

    public String getDrawToken() {
        return drawToken;
    }

    public void setDrawToken(String drawToken) {
        this.drawToken = drawToken;
    }
}