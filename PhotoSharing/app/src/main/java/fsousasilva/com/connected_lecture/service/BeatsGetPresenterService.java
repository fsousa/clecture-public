package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Presenter;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/22/16.
 */

public class BeatsGetPresenterService extends RetrofitSpiceRequest<Presenter, BeatsAPI> {

    private String token;

    public BeatsGetPresenterService(String token) {
        super(Presenter.class, BeatsAPI.class);
        this.token = token;
    }

    @Override
    public Presenter loadDataFromNetwork() throws Exception {
        return getService().getPresenter(getToken());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
