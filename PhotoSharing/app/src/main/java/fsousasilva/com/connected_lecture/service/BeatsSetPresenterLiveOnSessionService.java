package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Status;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 1/18/17.
 */

public class BeatsSetPresenterLiveOnSessionService extends RetrofitSpiceRequest<Status, BeatsAPI> {

    private String token;
    private String key;
    private boolean live;

    public BeatsSetPresenterLiveOnSessionService(String token, String key, boolean live) {
        super(Status.class, BeatsAPI.class);
        this.token = token;
        this.key = key;
        this.live = live;
    }

    @Override
    public Status loadDataFromNetwork() throws Exception {
        return getService().sessionSetPresenterLive(getToken(), getKey(), isLive());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }
}
