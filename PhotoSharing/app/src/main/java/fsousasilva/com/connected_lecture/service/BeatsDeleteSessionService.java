package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Status;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 12/14/16.
 */

public class BeatsDeleteSessionService extends RetrofitSpiceRequest<Status, BeatsAPI> {

    private String token;
    private String key;

    public BeatsDeleteSessionService(String token, String key) {
        super(Status.class, BeatsAPI.class);
        this.token = token;
        this.key = key;
    }

    @Override
    public Status loadDataFromNetwork() throws Exception {
        return getService().deleteSession(getToken(), getKey());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
