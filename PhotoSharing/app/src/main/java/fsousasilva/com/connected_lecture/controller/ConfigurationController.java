package fsousasilva.com.connected_lecture.controller;

import fsousasilva.com.connected_lecture.BuildConfig;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/23/16.
 */

public class ConfigurationController {


    public static final String DEV = BuildConfig.DEV;
    public static final String PROD = BuildConfig.PROD;

    public static final String APPMODELOCATION = "app_mode_location";
    public static final String APPMODEDEFAULT = PROD;
    public static final String APPMODE = "mode";


    private static ConfigurationController instance;

    private ConfigurationController() {
    }

    public static ConfigurationController getInstance() {
        if (instance == null) {
            instance = new ConfigurationController();
        }
        return instance;
    }

    public String getProtocol() {
        return "https://";
    }

    public String getServerFull() {
        return getProtocol() + getServer();
    }

    public String getServer() {
        return PROD;
    }

}
