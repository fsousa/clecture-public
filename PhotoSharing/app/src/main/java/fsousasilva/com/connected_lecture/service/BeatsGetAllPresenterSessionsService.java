package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.SessionShort;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 1/13/17.
 */

public class BeatsGetAllPresenterSessionsService extends RetrofitSpiceRequest<SessionShort.List, BeatsAPI> {

    private String token;

    public BeatsGetAllPresenterSessionsService(String token) {

        super(SessionShort.List.class, BeatsAPI.class);
        this.token = token;
    }

    @Override
    public SessionShort.List loadDataFromNetwork() throws Exception {
        return getService().getAllPresenterSessions(getToken());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
