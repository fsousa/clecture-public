package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 1/11/17.
 */

public class Feedback {

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("viewer")
    @Expose
    private Viewer viewer;

    @SerializedName("resource")
    @Expose
    private Resource resource;


    public Feedback(String description, Viewer viewer, Resource resource) {
        this.description = description;
        this.viewer = viewer;
        this.resource = resource;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Viewer getViewer() {
        return viewer;
    }

    public void setViewer(Viewer viewer) {
        this.viewer = viewer;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public static class List extends ArrayList<Feedback> {

    }
}
