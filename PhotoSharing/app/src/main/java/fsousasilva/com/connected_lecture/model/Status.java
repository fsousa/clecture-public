package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/22/16.
 */

public class Status {

    public static final String OK = "ok";
    @SerializedName("status")
    @Expose
    private String statusMsg;

    public Status() {
    }

    public Status(String statusMsg) {
        setStatusMsg(statusMsg);
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }
}
