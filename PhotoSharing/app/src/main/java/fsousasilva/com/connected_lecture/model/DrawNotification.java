package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/14/17.
 */

public class DrawNotification {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("session_key")
    @Expose
    private String sessionKey;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("x_offset")
    @Expose
    private float xOffset;
    @SerializedName("y_offset")
    @Expose
    private float yOffset;
    @SerializedName("zoom")
    @Expose
    private float zoom;

    public DrawNotification(int id, String sessionKey, String url, String xOffset, String yOffset, String zoom) {
        this.id = id;
        this.sessionKey = sessionKey;
        this.url = url;
        this.xOffset = Float.valueOf(xOffset);
        this.yOffset = Float.valueOf(yOffset);
        this.zoom = Float.valueOf(zoom);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public float getxOffset() {
        return xOffset;
    }

    public void setxOffset(float xOffset) {
        this.xOffset = xOffset;
    }

    public float getyOffset() {
        return yOffset;
    }

    public void setyOffset(float yOffset) {
        this.yOffset = yOffset;
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }
}
