package fsousasilva.com.connected_lecture.view;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 12/19/16.
 */

public class BaseActivity extends AppCompatActivity {

    private ProgressDialog mProgress;


    public void showProgressDialog() {
        showProgressDialog("Loading");
    }

    public void showProgressDialog(String title) {
        if (mProgress == null) {
            mProgress = ProgressDialog.show(this, title, "Just a moment...");
        }else {
            mProgress.show();
        }
    }

    public void hideProgressDialog() {
        if( mProgress != null) {
            mProgress.dismiss();
        }
    }
}
