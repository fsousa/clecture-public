package fsousasilva.com.connected_lecture.view;

import android.os.Bundle;
import android.widget.TextView;

import com.octo.android.robospice.SpiceManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import fsousasilva.com.connected_lecture.R;
import fsousasilva.com.connected_lecture.controller.UserController;
import fsousasilva.com.connected_lecture.model.User;
import fsousasilva.com.connected_lecture.service.RetrofitSpiceService;

public class SettingsActivity extends BaseActivity {

    @BindView(R.id.settingsUsernameTextVIew)
    TextView userNameTextView;

    private SpiceManager mSpiceManager = new SpiceManager(RetrofitSpiceService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);
    }

    public void updateFieldsOnScreen(User user) {
        userNameTextView.setText(user.getUsername());
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateFieldsOnScreen(UserController.getInstance().getUser(getApplicationContext()));

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mSpiceManager.isStarted()) {
            mSpiceManager.start(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }
    }

}
