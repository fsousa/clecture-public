package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Annotation;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/14/17.
 */

public class BeatsGetAnnotationService extends RetrofitSpiceRequest<Annotation, BeatsAPI> {

    private String token;
    private int annotationId;

    public BeatsGetAnnotationService(String token, int annotationId) {
        super(Annotation.class, BeatsAPI.class);
        this.token = token;
        this.annotationId = annotationId;
    }

    @Override
    public Annotation loadDataFromNetwork() throws Exception {
        return getService().getAnnotation(getToken(), getAnnotationId());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getAnnotationId() {
        return annotationId;
    }

    public void setAnnotationId(int annotationId) {
        this.annotationId = annotationId;
    }
}