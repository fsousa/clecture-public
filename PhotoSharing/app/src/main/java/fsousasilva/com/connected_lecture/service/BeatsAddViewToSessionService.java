package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Viewer;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/22/16.
 */

public class BeatsAddViewToSessionService extends RetrofitSpiceRequest<Viewer, BeatsAPI> {

    private String token;
    private String key;
    private String secret;

    public BeatsAddViewToSessionService( String token, String key, String secret) {
        super(Viewer.class, BeatsAPI.class);
        this.token = token;
        this.key = key;
        this.secret = secret;
    }

    @Override
    public Viewer loadDataFromNetwork() throws Exception {
        return getService().addViewToSession(getToken(), getKey(), getSecret());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
