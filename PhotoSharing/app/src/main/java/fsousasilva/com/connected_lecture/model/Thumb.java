package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 12/14/16.
 */

public class Thumb {

    @SerializedName("url")
    @Expose
    private String url;

    public Thumb(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
