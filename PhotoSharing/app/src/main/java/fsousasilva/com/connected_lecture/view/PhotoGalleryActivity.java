package fsousasilva.com.connected_lecture.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import fsousasilva.com.connected_lecture.R;
import fsousasilva.com.connected_lecture.controller.AuthController;
import fsousasilva.com.connected_lecture.model.Authenticate;
import fsousasilva.com.connected_lecture.model.Media;
import fsousasilva.com.connected_lecture.model.Resource;
import fsousasilva.com.connected_lecture.model.Session;
import fsousasilva.com.connected_lecture.model.Status;
import fsousasilva.com.connected_lecture.service.BeatsCreateResourceService;
import fsousasilva.com.connected_lecture.service.BeatsDeleteSessionService;
import fsousasilva.com.connected_lecture.service.BeatsSetSessionReadyService;
import fsousasilva.com.connected_lecture.service.RetrofitSpiceService;
import retrofit.mime.TypedFile;

public class PhotoGalleryActivity extends BaseActivity {

    @BindView(R.id.imageGridView)
    GridView imageGridView;
    @BindView(R.id.selectImageButton)
    Button selectImageButton;
    @BindView(R.id.galleryDoneButton)
    Button galleryDoneButton;
    @BindView(R.id.empty_list_item)
    TextView emptyImagesSelected;


    private ArrayList<Media> photosList = new ArrayList<>();
    private ImageAdapter myImageAdapter;

    private volatile int totalNumberOfResourcesToAdd = 0;
    private Session session;

    private SpiceManager mSpiceManager = new SpiceManager(RetrofitSpiceService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_gallery);

        String sessionObject = getIntent().getStringExtra("sessionObject");
        Gson gson = new Gson();
        session = gson.fromJson(sessionObject, Session.class);

        ButterKnife.bind(this);

        myImageAdapter = new ImageAdapter(this);
        imageGridView.setAdapter(myImageAdapter);
        imageGridView.setEmptyView(emptyImagesSelected);
        imageGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(PhotoGalleryActivity.this, "" + position,
                        Toast.LENGTH_SHORT).show();
            }
        });

        imageGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                photosList.remove(position);
                myImageAdapter.notifyDataSetChanged();
                return false;
            }
        });

        selectImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilePickerBuilder.getInstance().setMaxCount(1)
                        .setSelectedFiles(getPathsFromMedia(photosList))
                        .setActivityTheme(R.style.AppTheme)
                        .pickDocument(PhotoGalleryActivity.this);
            }
        });

        galleryDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (photosList.size() != 0) {
                    showProgressDialog("Uploading Files");
                }

                for (int i = 0; i < photosList.size(); i++) {
                    Media item = photosList.get(i);
                    TypedFile photo = new TypedFile(item.getType().toString(), new File(item.getPath()));
                    String name = photo.fileName();
                    fireCreateResources(name, photo, i);
                }
            }
        });

    }

    public void fireCreateResources(String name, TypedFile photo, Integer position) {
        Authenticate authenticate = AuthController.getInstance().getAuth(getApplicationContext());
        BeatsCreateResourceService createResource = new BeatsCreateResourceService(authenticate.getToken(), photo, "application/pdf", name, session.getKey(), position);
        mSpiceManager.execute(createResource, String.format("createResource-%s", name), DurationInMillis.ALWAYS_EXPIRED, new CreateResourceRequestListener());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mSpiceManager.isStarted()) {
            mSpiceManager.start(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    List<String> resultList = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS);
                    for (int i = 0; i < resultList.size(); i++) {
                        photosList.add(new Media(resultList.get(i), Media.MediaType.PDF));
                    }
                    myImageAdapter.notifyDataSetChanged();
                    totalNumberOfResourcesToAdd = photosList.size();
                }
                break;
        }

    }

    @Override
    public void onBackPressed() {
        fireDeleteSession();
    }

    public void fireDeleteSession() {
        showProgressDialog();
        Authenticate authenticate = AuthController.getInstance().getAuth(getApplicationContext());
        BeatsDeleteSessionService createResource = new BeatsDeleteSessionService(authenticate.getToken(), session.getKey());
        mSpiceManager.execute(createResource, "deleteSession", DurationInMillis.ALWAYS_EXPIRED, new DeleteSessionRequestListener());
    }

    public void fireSetSessionReady() {
        showProgressDialog();
        Authenticate authenticate = AuthController.getInstance().getAuth(getApplicationContext());
        BeatsSetSessionReadyService setSessionReady = new BeatsSetSessionReadyService(authenticate.getToken(), session.getKey());
        mSpiceManager.execute(setSessionReady, "setSessionReady", DurationInMillis.ALWAYS_EXPIRED, new SessionReadyRequestListener());
    }

    private ArrayList<String> getPathsFromMedia(List<Media> medias) {

        ArrayList<String> result = new ArrayList<>(medias.size());
        for (int i = 0; i < medias.size(); i++) {
            result.add(medias.get(i).getPath());
        }

        return result;
    }

    private synchronized void decrementNumberOfPictureToUpload() {
        totalNumberOfResourcesToAdd -= 1;
    }

    public final class CreateResourceRequestListener implements RequestListener<Resource> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(PhotoGalleryActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Resource resource) {

            decrementNumberOfPictureToUpload();

            if (totalNumberOfResourcesToAdd == 0) {
                hideProgressDialog();
                fireSetSessionReady();

            }

        }

    }

    public final class SessionReadyRequestListener implements RequestListener<Status> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(PhotoGalleryActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Status status) {

            hideProgressDialog();

            Gson gson = new Gson();
            Intent intent = new Intent(PhotoGalleryActivity.this, DisplayPhotosActivity.class);

            String sessionString = gson.toJson(session);

            Bundle bundle = new Bundle();
            bundle.putStringArrayList("photosList", getPathsFromMedia(photosList));

            intent.putExtras(bundle);
            intent.putExtra("sessionObject", sessionString);
            PhotoGalleryActivity.this.startActivity(intent);
            PhotoGalleryActivity.this.finish();


        }

    }

    public final class DeleteSessionRequestListener implements RequestListener<Status> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(PhotoGalleryActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Status status) {

            hideProgressDialog();
            PhotoGalleryActivity.super.onBackPressed();

        }

    }

    private class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ImageAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return photosList.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {


            ImageView imageView;
            TextView fileName;
            if (convertView == null) {
                //We must create a View:
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.gallery_photo_row, parent, false);
            }

            Media item = photosList.get(position);

            imageView = (ImageView) convertView.findViewById(R.id.galleryRowImageView);
            fileName = (TextView) convertView.findViewById(R.id.galleryRowTextView);

            fileName.setText(item.getPath());

            return convertView;

        }
    }

}
