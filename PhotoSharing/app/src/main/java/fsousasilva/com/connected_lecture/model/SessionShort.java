package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 12/14/16.
 */

public class SessionShort {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("is_open")
    @Expose
    private Boolean isOpen;
    @SerializedName("presenter")
    @Expose
    private Presenter presenter;
    @Expose
    @SerializedName("number_of_viewers")
    private Integer numberOfViewer;
    @SerializedName("live")
    @Expose
    private boolean live;
    @SerializedName("distance")
    @Expose
    private float distance;

    private String secret;

    public SessionShort(String name, String key, Boolean isOpen, Presenter presenter, Integer numberOfViewer, boolean live, float distance) {
        this.name = name;
        this.key = key;
        this.isOpen = isOpen;
        this.presenter = presenter;
        this.numberOfViewer = numberOfViewer;
        this.live = live;
        this.distance = distance;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public Presenter getPresenter() {
        return presenter;
    }

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Integer getNumberOfViewer() {
        return numberOfViewer;
    }

    public void setNumberOfViewer(Integer numberOfViewer) {
        this.numberOfViewer = numberOfViewer;
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public static class List extends ArrayList<SessionShort> {

    }
}
