package fsousasilva.com.connected_lecture.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.agsw.FabricView.FabricView;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;

import br.net.bmobile.websocketrails.WebSocketRailsChannel;
import br.net.bmobile.websocketrails.WebSocketRailsDataCallback;
import br.net.bmobile.websocketrails.WebSocketRailsDispatcher;
import butterknife.BindView;
import butterknife.ButterKnife;
import fsousasilva.com.connected_lecture.R;
import fsousasilva.com.connected_lecture.controller.AuthController;
import fsousasilva.com.connected_lecture.controller.ConfigurationController;
import fsousasilva.com.connected_lecture.controller.UserController;
import fsousasilva.com.connected_lecture.model.Authenticate;
import fsousasilva.com.connected_lecture.model.DrawLeaseToken;
import fsousasilva.com.connected_lecture.model.DrawNotification;
import fsousasilva.com.connected_lecture.model.Feedback;
import fsousasilva.com.connected_lecture.model.Media;
import fsousasilva.com.connected_lecture.model.NotificationTypeEnum;
import fsousasilva.com.connected_lecture.model.PDFDraw;
import fsousasilva.com.connected_lecture.model.Resource;
import fsousasilva.com.connected_lecture.model.Session;
import fsousasilva.com.connected_lecture.model.Status;
import fsousasilva.com.connected_lecture.model.Task;
import fsousasilva.com.connected_lecture.model.User;
import fsousasilva.com.connected_lecture.service.BeatsCreateAnnotationService;
import fsousasilva.com.connected_lecture.service.BeatsCreateFeedbackService;
import fsousasilva.com.connected_lecture.service.BeatsCreateNotificationService;
import fsousasilva.com.connected_lecture.service.BeatsGetFeedbacksService;
import fsousasilva.com.connected_lecture.service.BeatsRemoveViewFromSession;
import fsousasilva.com.connected_lecture.service.RetrofitSpiceService;
import retrofit.mime.TypedFile;
import utils.DownloadFileUtil;
import utils.FileUtil;
import utils.ResourceComparator;

public class DisplayPhotosViewerActivity extends BaseActivity implements OnDrawListener {

    @BindView(R.id.viewer_pdf_view)
    PDFView mDemoSlider;


    //Menu Request
    @BindView(R.id.viewer_action_raise_hand_button)
    FloatingActionButton raiseHandButton;
    @BindView(R.id.viewer_action_go_to_page_button)
    FloatingActionButton goToButton;
    @BindView(R.id.viewer_action_ask_permission_to_draw_page_button)
    FloatingActionButton askPermissionToDraw;
    @BindView(R.id.viewer_action_option_menu)
    FloatingActionMenu floatingRequestMenu;


    //Menu Color
    @BindView(R.id.viewer_view_draw_color_menu)
    FloatingActionMenu floatingColorMenu;
    @BindView(R.id.viewer_view_draw_color_red_button)
    FloatingActionButton colorRedFloatingButton;
    @BindView(R.id.viewer_view_draw_color_green_button)
    FloatingActionButton colorGreenFloatingButton;
    @BindView(R.id.viewer_view_draw_color_blue_button)
    FloatingActionButton colorBlueFloatingButton;
    @BindView(R.id.viewer_view_draw_color_black_button)
    FloatingActionButton colorBlackFloatingButton;

    //Menu More
    @BindView(R.id.viewer_view_draw_more_menu)
    FloatingActionMenu floatingMoreMenu;
    @BindView(R.id.viewer_view_draw_clean_button)
    FloatingActionButton cleanDrawButton;
    @BindView(R.id.viewer_view_draw_send_button)
    FloatingActionButton sendDrawButton;

    @BindView(R.id.viewer_fabricView)
    FabricView drawCanvas;

    private WebSocketRailsDispatcher dispatcher;
    private WebSocketRailsChannel webSocketRailsChannel;
    private WebSocketRailsChannel webSocketRailsDrawChannel;
    private WebSocketRailsChannel webSocketRailsDrawLeaseChannel;
    private Session mySession;
    private Paint mPaint;
    private String mDrawToken;

    private SpiceManager mSpiceManager = new SpiceManager(RetrofitSpiceService.class);

    private SparseArray<PDFDraw> drawingMap = new SparseArray<>();

    private MaterialDialog permissionDrawPopup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_photos_viewer);

        ButterKnife.bind(this);

        String sessionObject = getIntent().getStringExtra("sessionObject");
        Gson gson = new Gson();
        mySession = gson.fromJson(sessionObject, Session.class);


        Collections.sort(mySession.getResources(), new ResourceComparator());

        subscribeToWebsocket();

        //TODO
        //By now we only add one resource
        for (Resource resource : mySession.getResources()) {
            downloadFiles(resource.getPicture().getUrl());
            break;
        }

        raiseHandButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                floatingRequestMenu.close(true);
                fireCreateNotification(NotificationTypeEnum.RAISE_HAND, "");
            }
        });

        goToButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                floatingRequestMenu.close(true);
                new MaterialDialog.Builder(DisplayPhotosViewerActivity.this)
                        .title("Go to Page")
                        .content("Type the page number")
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .input("Page Number", "", new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                if (!input.toString().equals("")) {
                                    fireCreateNotification(NotificationTypeEnum.GOTO, input.toString());
                                }
                            }
                        }).show();

            }
        });

        askPermissionToDraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                floatingRequestMenu.close(true);
                UserController userController = UserController.getInstance();
                User user = userController.getUser(DisplayPhotosViewerActivity.this);
                fireCreateNotification(NotificationTypeEnum.ASK_PERMISSION_TO_DRAW, user.getId());
            }
        });

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        permissionDrawPopup = new MaterialDialog.Builder(DisplayPhotosViewerActivity.this)
                .title("Waiting Permission")
                .content("The lecturer must approve your request. Please wait.")
                .progress(true, 0).build();

    }


    @Override
    public void onStart() {
        super.onStart();
        if (!mSpiceManager.isStarted()) {
            mSpiceManager.start(this);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }

        if (dispatcher != null) {
            dispatcher.disconnect();
        }
    }

    @Override
    public void onBackPressed() {
        fireRemoveViewFromSession();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_viewer_display, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_view_feedback) {
            fireGetFeedbacks();
            return true;
        } else if (id == R.id.action_add_feedback) {
            addFeedbackPopup();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void downloadFiles(String url) {
        showProgressDialog("Downloading Files");

        DownloadFileUtil downloadFileUtil = new DownloadFileUtil(this, new DownloadStatusListenerV1() {
            @Override
            public void onDownloadComplete(DownloadRequest downloadRequest) {
                hideProgressDialog();
                finishPdfConfiguration(mDemoSlider.fromFile(new File(downloadRequest.getDestinationURI().getPath())));
            }

            @Override
            public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {
                Toast.makeText(DisplayPhotosViewerActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                hideProgressDialog();

            }

            @Override
            public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {

            }
        });

        downloadFileUtil.downloadFiles(url);
    }

    private void finishPdfConfiguration(PDFView.Configurator conf) {
        assert conf != null;
        conf.defaultPage(mySession.getCurrentPage()).enableSwipe(false).swipeHorizontal(true).onDraw(this).load();

        setupFloatingColorButtons();
        setupFloatingMoreButtons();
    }

    private void addFeedbackPopup() {

        new MaterialDialog.Builder(this)
                .title("Add Comment")
                .content("Type Your Comment")
                .inputRangeRes(1, 140, R.color.md_material_blue_600)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input("Comment", "", new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        if (!input.toString().equals("")) {
                            fireCreateFeedback(input.toString());
                        }
                    }
                }).show();

    }

    private void setupFloatingColorButtons() {
        colorBlackFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawCanvas.setColor(Color.BLACK);
                floatingColorMenu.close(true);
            }
        });

        colorRedFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawCanvas.setColor(Color.RED);
                floatingColorMenu.close(true);
            }
        });

        colorGreenFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawCanvas.setColor(Color.GREEN);
                floatingColorMenu.close(true);
            }
        });

        colorBlueFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawCanvas.setColor(Color.BLUE);
                floatingColorMenu.close(true);
            }
        });

    }

    private void setupFloatingMoreButtons() {
        sendDrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopDrawing();
            }
        });

        cleanDrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawCanvas.cleanPage();
            }
        });
    }

    private void startDrawing() {
        floatingRequestMenu.setVisibility(View.GONE);
        floatingMoreMenu.setVisibility(View.VISIBLE);
        floatingColorMenu.setVisibility(View.VISIBLE);

        drawCanvas.setVisibility(View.VISIBLE);
        drawCanvas.setColor(Color.RED);
        drawCanvas.setBackgroundColor(Color.TRANSPARENT);

    }

    private void stopDrawing() {

        floatingColorMenu.close(false);
        floatingMoreMenu.close(false);

        floatingRequestMenu.setVisibility(View.VISIBLE);
        floatingMoreMenu.setVisibility(View.GONE);
        floatingColorMenu.setVisibility(View.GONE);

        Bitmap bitmap = drawCanvas.getCanvasBitmap();
        int page = mDemoSlider.getCurrentPage();
        float xOffset = mDemoSlider.getCurrentXOffset();
        float yOffset = mDemoSlider.getCurrentYOffset();
        float zoom = mDemoSlider.getZoom();

        Bitmap emptyBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        if (!emptyBitmap.sameAs(bitmap)) {
            emptyBitmap.recycle();

            drawingMap.append(page, new PDFDraw(drawCanvas.getHeight(), drawCanvas.getWidth(), bitmap, xOffset, yOffset, zoom));

            FileUtil fileUtil = new FileUtil(DisplayPhotosViewerActivity.this);
            File bitmapFile = fileUtil.saveBitmapToTempFile("tempbitmap" + page + ".png", bitmap);

            if (bitmapFile != null) {
                fireCreateAnnotation("", bitmapFile, Integer.valueOf(getCurrentResourceId()), page, String.valueOf(xOffset), String.valueOf(yOffset), String.valueOf(zoom), mDrawToken);
            }


        }

        drawCanvas.cleanPage();
        drawCanvas.setVisibility(View.GONE);
        mDemoSlider.invalidate();

    }

    public void fireCreateAnnotation(String note, File bitmap, int resourceId, int page, String xOffset, String yOffset, String zoom, String drawToken) {

        TypedFile draw = new TypedFile(Media.MediaType.PNG.name(), bitmap);
        Authenticate auth = AuthController.getInstance().getAuth(DisplayPhotosViewerActivity.this);
        BeatsCreateAnnotationService createAnnotationService = new BeatsCreateAnnotationService(auth.getToken(), note, draw, resourceId, page, xOffset, yOffset, zoom, drawToken);
        mSpiceManager.execute(createAnnotationService, "fireCreateAnnotation", DurationInMillis.ALWAYS_EXPIRED, null);
    }

    private void fireCreateNotification(NotificationTypeEnum notificationType, String notificationContent) {
        Authenticate auth = AuthController.getInstance().getAuth(DisplayPhotosViewerActivity.this);
        BeatsCreateNotificationService getSessions = new BeatsCreateNotificationService(auth.getToken(), mySession.getKey(), notificationType, notificationContent);
        mSpiceManager.execute(getSessions, "fireCreateNotification", DurationInMillis.ALWAYS_EXPIRED, new CreateNotificationRequestListener(notificationType));
    }

    private void fireCreateFeedback(String feedback) {

        showProgressDialog();
        Authenticate auth = AuthController.getInstance().getAuth(DisplayPhotosViewerActivity.this);
        BeatsCreateFeedbackService getSessions = new BeatsCreateFeedbackService(auth.getToken(), mySession.getKey(), getCurrentResourceId(), feedback);
        mSpiceManager.execute(getSessions, "fireCreateFeedback", DurationInMillis.ALWAYS_EXPIRED, new CreateFeedbackRequestListener());
    }

    private void fireGetFeedbacks() {
        showProgressDialog();
        Authenticate auth = AuthController.getInstance().getAuth(DisplayPhotosViewerActivity.this);
        BeatsGetFeedbacksService getSessions = new BeatsGetFeedbacksService(auth.getToken(), mySession.getKey());
        mSpiceManager.execute(getSessions, "fireGetFeedbacks", DurationInMillis.ALWAYS_EXPIRED, new ViewFeedbackRequestListener());

    }

    private void fireRemoveViewFromSession() {
        Authenticate auth = AuthController.getInstance().getAuth(DisplayPhotosViewerActivity.this);
        BeatsRemoveViewFromSession removeView = new BeatsRemoveViewFromSession(auth.getToken(), mySession.getKey());
        mSpiceManager.execute(removeView, "fireRemoveViewFromSession", DurationInMillis.ALWAYS_EXPIRED, new RemoveViewFromSessionRequestListener());
    }

    private String getCurrentResourceId() {
        //TODO
        //Only handle one resource
        return mySession.getResources().get(0).getId();
    }

    private Paint getPaintAreaForBitmap() {
        if (mPaint == null) {
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setFilterBitmap(true);
            mPaint.setDither(true);
        }

        return mPaint;
    }

    @Override
    public void onLayerDrawn(Canvas canvas, float pageWidth, float pageHeight, int displayedPage) {

        PDFDraw draw = drawingMap.get(displayedPage);
        if (draw != null) {

            Paint paint = getPaintAreaForBitmap();
            float currentZoom = mDemoSlider.getZoom();

            if (currentZoom != draw.getZoom()) {

                float zoom = currentZoom / draw.getZoom();

                int wantedW = (int) (zoom * draw.getWidth());
                int wantedH = (int) (zoom * draw.getHeight());

                float offSetX = zoom * draw.getOffSetX();
                float offSetY = zoom * draw.getOffSetY();

                Bitmap bitmap = Bitmap.createScaledBitmap(draw.getImage(), wantedW, wantedH, false);
                canvas.drawBitmap(bitmap, -offSetX / pageWidth, -offSetY, paint);
                bitmap.recycle();

            } else {

                float offSetX = draw.getOffSetX();
                float offSetY = draw.getOffSetY();

                canvas.drawBitmap(draw.getImage(), -offSetX / pageWidth, -offSetY, paint);
            }
        }
    }

    public void subscribeToWebsocket() {

        try {
            dispatcher = new WebSocketRailsDispatcher(new URL(ConfigurationController.getInstance().getServerFull() + "/websocket"));
            dispatcher.connect();

            UserController userController = UserController.getInstance();
            User user = userController.getUser(DisplayPhotosViewerActivity.this);

            webSocketRailsDrawLeaseChannel = dispatcher.subscribe("notifyviewer");
            webSocketRailsDrawLeaseChannel.bind(user.getId(), new WebSocketRailsDataCallback() {
                @Override
                public void onDataAvailable(Object data) {
                    Gson gson = new Gson();
                    final DrawLeaseToken drawLeaseNotification = gson.fromJson(data.toString(), DrawLeaseToken.class);

                    Handler uiHandler = new Handler(Looper.getMainLooper());
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            permissionDrawPopup.dismiss();
                            if (drawLeaseNotification.getDrawToken().equals("error")) {
                                mDrawToken = null;
                                showPositivePopup("Ask to Draw", "The lecturer denied your request.");
                            } else {
                                mDrawToken = drawLeaseNotification.getDrawToken();
                                startDrawing();
                            }
                        }
                    };
                    uiHandler.post(runnable);

                }
            });

            webSocketRailsDrawChannel = dispatcher.subscribe("draw");
            webSocketRailsDrawChannel.bind(mySession.getKey(), new WebSocketRailsDataCallback() {
                @Override
                public void onDataAvailable(Object data) {
                    Gson gson = new Gson();
                    final DrawNotification drawNotification = gson.fromJson(data.toString(), DrawNotification.class);

                    ImageLoader imageLoader = ImageLoader.getInstance();
                    imageLoader.loadImage(drawNotification.getUrl(), new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            Bitmap bitmap = Bitmap.createScaledBitmap(loadedImage, mDemoSlider.getWidth(), mDemoSlider.getHeight(), false);
                            PDFDraw item = new PDFDraw(bitmap.getHeight(), bitmap.getWidth(), bitmap, mDemoSlider.getCurrentXOffset(), mDemoSlider.getCurrentYOffset(), drawNotification.getZoom());
                            drawingMap.append(mDemoSlider.getCurrentPage(), item);

                            Handler uiHandler = new Handler(Looper.getMainLooper());
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    mDemoSlider.invalidate();
                                }
                            };
                            uiHandler.post(runnable);
                        }
                    });
                }
            });

            webSocketRailsChannel = dispatcher.subscribe("task");
            webSocketRailsChannel.bind(mySession.getKey(), new WebSocketRailsDataCallback() {

                @Override
                public void onDataAvailable(Object data) {
                    Gson gson = new Gson();
                    final Task task = gson.fromJson(data.toString(), Task.class);

                    Handler uiHandler = new Handler(Looper.getMainLooper());
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {

                            if (mDemoSlider.getZoom() != task.getZoom()) {
                                mDemoSlider.zoomTo(task.getZoom());
                            }
                            if (mDemoSlider.getPositionOffset() != task.getPositionOffSet()) {
                                mDemoSlider.setPositionOffset(task.getPositionOffSet());
                            }
                            if (mySession.getCurrentPage() != task.getCurrentPage()) {
                                mySession.setCurrentPage(task.getCurrentPage());
                            }

                        }
                    };
                    uiHandler.post(runnable);
                }
            });
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void showPositivePopup(String title, String content) {
        new MaterialDialog.Builder(DisplayPhotosViewerActivity.this)
                .title(title)
                .content(content)
                .positiveText("Ok")
                .show();
    }

    public final class RemoveViewFromSessionRequestListener implements RequestListener<Status> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(DisplayPhotosViewerActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Status status) {
            hideProgressDialog();
            DisplayPhotosViewerActivity.super.onBackPressed();
        }
    }

    public final class CreateNotificationRequestListener implements RequestListener<Status> {

        private NotificationTypeEnum notificationType;

        CreateNotificationRequestListener(NotificationTypeEnum notificationType) {
            this.notificationType = notificationType;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(DisplayPhotosViewerActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Status status) {
            if (notificationType.equals(NotificationTypeEnum.ASK_PERMISSION_TO_DRAW)) {
                permissionDrawPopup.show();
            }
        }
    }

    public final class ViewFeedbackRequestListener implements RequestListener<Feedback.List> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(DisplayPhotosViewerActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Feedback.List feedbacks) {
            hideProgressDialog();

            if (feedbacks.size() != 0) {
                new MaterialDialog.Builder(DisplayPhotosViewerActivity.this)
                        .title("Comments List")
                        .adapter(new FeedbackAdapter(feedbacks), null)
                        .positiveText("Close")
                        .show();
            } else {
                showPositivePopup("No Comment", "This document has no comment yet");
            }
        }
    }

    public final class CreateFeedbackRequestListener implements RequestListener<Status> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(DisplayPhotosViewerActivity.this, spiceException.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onRequestSuccess(Status status) {
            hideProgressDialog();
        }
    }

    private class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {
        private Feedback.List myDataset;

        public FeedbackAdapter(Feedback.List myDataset) {
            this.myDataset = myDataset;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.description.setText(myDataset.get(position).getDescription());
            holder.viewerName.setText(myDataset.get(position).getViewer().getName());

        }

        @Override
        public int getItemCount() {
            return myDataset.size();
        }

        @Override
        public FeedbackAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.feedbacks_row, parent, false);

            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            TextView description;
            TextView viewerName;

            public ViewHolder(View v) {
                super(v);
                description = (TextView) v.findViewById(R.id.feedbacksDescriptionRowTextView);
                viewerName = (TextView) v.findViewById(R.id.feedbacksViewerRowTextView);
            }
        }
    }


}
