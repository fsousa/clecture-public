package fsousasilva.com.connected_lecture.service;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

import fsousasilva.com.connected_lecture.controller.ConfigurationController;
import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/22/16.
 */

public class RetrofitSpiceService extends RetrofitGsonSpiceService {


    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(BeatsAPI.class);
    }


    @Override
    protected String getServerUrl() {
        return ConfigurationController.getInstance().getServerFull();
    }

    @Override
    protected Converter createConverter() {

        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return new GsonConverter(gson);
    }

    @Override
    public int getThreadCount() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getApplicationContext().getSystemService(CONNECTIVITY_SERVICE);

        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if (info == null) {
            return 2; // there is no network available now. Anyway we use the default num of thread
        }
        switch (info.getType()) {
            case ConnectivityManager.TYPE_WIFI:
            case ConnectivityManager.TYPE_WIMAX:
            case ConnectivityManager.TYPE_ETHERNET:
                return 3;
            case ConnectivityManager.TYPE_MOBILE:
                return 2;
            default:
                return 2;
        }
    }


}
