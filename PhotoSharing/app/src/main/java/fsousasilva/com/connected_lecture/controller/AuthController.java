package fsousasilva.com.connected_lecture.controller;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import fsousasilva.com.connected_lecture.model.Authenticate;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/23/16.
 */

public class AuthController {

    private final String AUTHOBJECT = "photosharing_shared_auth_object";
    private final String AUTHKEY = "photosharing_shared_auth_keyt";

    public static AuthController mInstance;

    private AuthController() {
    }

    public static AuthController getInstance() {
        if (mInstance == null) {
            mInstance = new AuthController();
        }
        return mInstance;
    }

    public void saveAuth(Activity ctx, Authenticate authenticate) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(AUTHKEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new Gson();
        String auth = gson.toJson(authenticate);
        editor.putString(AUTHOBJECT, auth);
        editor.apply();
    }

    public Authenticate getAuth(Context ctx) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(AUTHKEY, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPref.getString(AUTHOBJECT, "");
        return gson.fromJson(json, Authenticate.class);
    }

    public void cleanAuth(Context ctx) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(AUTHKEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(AUTHOBJECT);
        editor.apply();
    }
}
