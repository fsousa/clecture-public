package fsousasilva.com.connected_lecture.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.text.DecimalFormat;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import fsousasilva.com.connected_lecture.R;
import fsousasilva.com.connected_lecture.controller.AuthController;
import fsousasilva.com.connected_lecture.model.Authenticate;
import fsousasilva.com.connected_lecture.model.SessionShort;
import fsousasilva.com.connected_lecture.model.Status;
import fsousasilva.com.connected_lecture.service.BeatsCheckSessionSecretService;
import fsousasilva.com.connected_lecture.service.BeatsGetSessionsService;
import fsousasilva.com.connected_lecture.service.RetrofitSpiceService;
import utils.SessionComparator;

public class ViewerActivity extends BaseActivity {

    @BindView(R.id.sessionsGridView)
    GridView sessionsGridView;
    @BindView(R.id.sessionUpdatebutton)
    Button refreshButton;
    @BindView(R.id.empty_list_item)
    TextView emptySessionList;

    private final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 10;

    private SpiceManager mSpiceManager = new SpiceManager(RetrofitSpiceService.class);

    private SessionShort.List mySessions = new SessionShort.List();
    private SessionAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer);

        ButterKnife.bind(this);


        myAdapter = new SessionAdapter(getApplicationContext());
        sessionsGridView.setAdapter(myAdapter);
        sessionsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sessionSelected(position);
            }
        });
        sessionsGridView.setEmptyView(emptySessionList);

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fireGetSessionsCall();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        fireGetSessionsCall();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mSpiceManager.isStarted()) {
            mSpiceManager.start(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(ViewerActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(ViewerActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                Log.i("APP", "We need your permission");

            } else {

                ActivityCompat.requestPermissions(ViewerActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

            }
        }
    }

    @Nullable
    private Location getUserLocation() {

        if (ContextCompat.checkSelfPermission(ViewerActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            return lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        } else {
            return null;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.i("APP", "Permission granted");

                } else {

                    Log.i("APP", "Create session cancelled. Permission denied");
                }
            }
        }
    }

    private void sessionSelected(int position) {
        final SessionShort session = mySessions.get(position);

        if (!session.getOpen()) {
            new MaterialDialog.Builder(this)
                    .title("Lecture Details")
                    .content("Type Lecture Secret")
                    .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                    .input("Lecture Secret", "", new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(MaterialDialog dialog, CharSequence input) {
                            fireCheckSessionSecret(session, input.toString());
                        }
                    }).show();
        } else {
            goToSessionDetails(session);
        }


    }

    private void fireGetSessionsCall() {

        showProgressDialog("Loading Lectures");

        Location location = getUserLocation();
        String lat = "";
        String lng = "";
        if (location != null) {
            lat = String.valueOf(location.getLatitude());
            lng = String.valueOf(location.getLongitude());
        }

        Authenticate auth = AuthController.getInstance().getAuth(ViewerActivity.this);
        BeatsGetSessionsService getSessions = new BeatsGetSessionsService(auth.getToken(), lat, lng);
        mSpiceManager.execute(getSessions, "sessions", DurationInMillis.ALWAYS_EXPIRED, new SessionRequestListener());

    }

    private void fireCheckSessionSecret(SessionShort session, String secret) {
        showProgressDialog();
        Authenticate auth = AuthController.getInstance().getAuth(ViewerActivity.this);
        BeatsCheckSessionSecretService getSessions = new BeatsCheckSessionSecretService(auth.getToken(), session.getKey(), secret);
        mSpiceManager.execute(getSessions, "checkSessionSecret", DurationInMillis.ALWAYS_EXPIRED, new CheckSessionSecretRequestListener(session, secret));
    }

    private void goToSessionDetails(SessionShort session) {
        Gson gson = new Gson();
        String sessionString = gson.toJson(session);
        Intent intent = new Intent(ViewerActivity.this, SessionDetailsActivity.class);
        intent.putExtra("sessionObject", sessionString);
        ViewerActivity.this.startActivity(intent);
    }

    public final class SessionRequestListener implements RequestListener<SessionShort.List> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(ViewerActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(SessionShort.List sessions) {
            hideProgressDialog();
            mySessions = sessions;
            Collections.sort(mySessions, new SessionComparator());
            myAdapter.notifyDataSetChanged();
        }

    }

    public final class CheckSessionSecretRequestListener implements RequestListener<Status> {


        private SessionShort session;
        private String secret;

        public CheckSessionSecretRequestListener(SessionShort session, String secret) {
            this.session = session;
            this.secret = secret;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(ViewerActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Status status) {
            hideProgressDialog();
            SessionShort innerSession = getSession();
            innerSession.setSecret(secret);
            goToSessionDetails(innerSession);
        }

        public SessionShort getSession() {
            return session;
        }

        public void setSession(SessionShort session) {
            this.session = session;
        }
    }

    private class SessionAdapter extends BaseAdapter {
        private Context mContext;

        public SessionAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return mySessions.size();
        }

        public Object getItem(int position) {
            return mySessions.get(position);
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {


            TextView nameTextView;
            TextView presenterTextView;
            ImageView lockerImageView;
            TextView live;
            TextView distance;
            if (convertView == null) {
                //We must create a View:
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.session_row, parent, false);
            }

            SessionShort session = mySessions.get(position);
            nameTextView = (TextView) convertView.findViewById(R.id.sessionRowTextView);
            nameTextView.setText(session.getName());

            presenterTextView = (TextView) convertView.findViewById(R.id.sessionRowPresentertextView);
            presenterTextView.setText(session.getPresenter().getName());

            lockerImageView = (ImageView) convertView.findViewById(R.id.sessionRowLockimageView);
            live = (TextView) convertView.findViewById(R.id.sessionRowLiveTextView);
            distance = (TextView) convertView.findViewById(R.id.sessionRowDistanceTextView);

            if (session.getOpen()) {
                lockerImageView.setImageResource(R.mipmap.unlocked);
            } else {
                lockerImageView.setImageResource(R.mipmap.locked);
            }

            if (session.isLive()) {
                live.setTextColor(Color.RED);
            }else {
                live.setTextColor(Color.GRAY);
            }

            if (session.getDistance() >= 10.0) {
                distance.setText("9+ km");
            }else {
                distance.setText(new DecimalFormat("#.# km").format(session.getDistance()));
            }


            return convertView;

        }
    }
}
