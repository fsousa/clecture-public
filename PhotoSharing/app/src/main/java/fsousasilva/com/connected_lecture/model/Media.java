package fsousasilva.com.connected_lecture.model;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/4/17.
 */

public class Media {

    public enum MediaType {
        PDF ("application/pdf") , JPG ("image/jpg"), PNG ("image/png");

        private final String name;

        MediaType(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }
    }

    private String path;
    private MediaType type;

    public Media(String path, MediaType type) {
        this.path = path;
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public MediaType getType() {
        return type;
    }

    public void setType(MediaType type) {
        this.type = type;
    }
}
