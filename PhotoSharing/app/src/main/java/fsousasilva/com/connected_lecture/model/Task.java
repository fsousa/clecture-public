package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 12/15/16.
 */

public class Task {

    @SerializedName("current_page")
    private int currentPage;
    @SerializedName("key")
    private String key;
    @SerializedName("zoom")
    private float zoom;
    @SerializedName("position_offset")
    private float positionOffSet;

    public Task(int currentPage, String key, String zoom, String positionOffSet) {
        this.currentPage = currentPage;
        this.key = key;
        this.zoom = Float.valueOf(zoom);
        this.positionOffSet = Float.valueOf(positionOffSet);

    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

    public float getPositionOffSet() {
        return positionOffSet;
    }

    public void setPositionOffSet(float positionOffSet) {
        this.positionOffSet = positionOffSet;
    }

}
