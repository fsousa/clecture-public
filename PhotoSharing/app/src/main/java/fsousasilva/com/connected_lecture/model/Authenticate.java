package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/22/16.
 */

public class Authenticate {

    public static String TABLE_NAME = "Authenticate";

    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("token")
    private String token;

    public Authenticate() {

    }

    public Authenticate(String status, String token, String network) {
        this.status = status;
        this.token = token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
