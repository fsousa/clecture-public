package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.User;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 1/19/17.
 */

public class BeatsGetUserProfileService extends RetrofitSpiceRequest<User, BeatsAPI> {

    private String token;

    public BeatsGetUserProfileService(String token) {
        super(User.class, BeatsAPI.class);
        this.token = token;
    }

    @Override
    public User loadDataFromNetwork() throws Exception {
        return getService().getUserProfile(getToken());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
