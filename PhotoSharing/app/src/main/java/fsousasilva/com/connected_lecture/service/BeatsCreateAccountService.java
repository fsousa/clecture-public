package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Authenticate;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/22/16.
 */

public class BeatsCreateAccountService extends RetrofitSpiceRequest<Authenticate, BeatsAPI> {

    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;

    public BeatsCreateAccountService(String username, String password, String firstName, String lastName, String email) {
        super(Authenticate.class, BeatsAPI.class);
        this.username = username.trim();
        this.password = password.trim();
        this.firstName = firstName.trim();
        this.lastName = lastName.trim();
        this.email = email.trim();
    }

    @Override
    public Authenticate loadDataFromNetwork() throws Exception {
        return getService().createAccount(getUsername(), getPassword(), getFirstName(), getLastName(), getEmail());
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
