package fsousasilva.com.connected_lecture.service;

import fsousasilva.com.connected_lecture.model.Annotation;
import fsousasilva.com.connected_lecture.model.Authenticate;
import fsousasilva.com.connected_lecture.model.DrawLeaseToken;
import fsousasilva.com.connected_lecture.model.Feedback;
import fsousasilva.com.connected_lecture.model.Presenter;
import fsousasilva.com.connected_lecture.model.Resource;
import fsousasilva.com.connected_lecture.model.Session;
import fsousasilva.com.connected_lecture.model.SessionShort;
import fsousasilva.com.connected_lecture.model.Status;
import fsousasilva.com.connected_lecture.model.User;
import fsousasilva.com.connected_lecture.model.Viewer;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;


public interface BeatsAPI {

    String VERSION = "/api/v1";

    @Multipart
    @POST(VERSION + "/signup/create.json")
    Authenticate createAccount(@Part("username") String userName, @Part("password") String password, @Part("first_name") String firstName, @Part("last_name") String lastName, @Part("email") String email);

    @Multipart
    @POST(VERSION + "/auth/login.json")
    Authenticate auth(@Part("username") String userName, @Part("password") String password);

    @GET(VERSION + "/auth/validate.json")
    Status validate(@Query("token") String token);

    @GET(VERSION + "/secured/presenters.json")
    Presenter.List getPresenters(@Query("token") String token);

    @GET(VERSION + "/secured/presenters/profile.json")
    Presenter getPresenter(@Query("token") String token);

    @Multipart
    @POST(VERSION + "/secured/presenters/create.json")
    Status createPresenter(@Part("token") String token, @Part("name") String name);

    @Multipart
    @POST(VERSION + "/secured/viewers/create.json")
    Status createViewer(@Part("token") String token, @Part("name") String name);

    @Multipart
    @POST(VERSION + "/secured/sessions/create.json")
    Session createSession(@Part("token") String token, @Part("name") String name, @Part("lat") String lat, @Part("lng") String lng, @Part("is_open") boolean isOpen, @Part("secret") String secret);

    @GET(VERSION + "/secured/sessions.json")
    SessionShort.List getSessions(@Query("token") String token, @Query("lat") String lat, @Query("lng") String lng);

    @GET(VERSION + "/secured/sessions/details.json")
    Session getSession(@Query("token") String token, @Query("key") String key, @Query("secret") String secret);

    @GET(VERSION + "/secured/sessions/presenter.json")
    SessionShort.List getAllPresenterSessions(@Query("token") String token);

    @DELETE(VERSION + "/secured/sessions.json")
    Status deleteSession(@Query("token") String token, @Query("key") String key);

    @Multipart
    @PUT(VERSION + "/secured/sessions/addViewToSession.json")
    Viewer addViewToSession(@Part("token") String token, @Part("key") String key, @Part("secret") String secret);

    @Multipart
    @PUT(VERSION + "/secured/sessions/removeViewFromSession.json")
    Status removeViewFromSession(@Part("token") String token, @Part("key") String key);

    @GET(VERSION + "/secured/sessions/checkSecret.json")
    Status checkSessionSecret(@Query("token") String token, @Query("key") String key, @Query("secret") String secret);

    @Multipart
    @POST(VERSION + "/secured/sessions/changePage.json")
    Status sessionChangePage(@Part("token") String token, @Part("key") String key, @Part("current_page") int currentPage);

    @Multipart
    @POST(VERSION + "/secured/sessions/changeZoom.json")
    Status sessionChangeZoom(@Part("token") String token, @Part("key") String key, @Part("zoom") String zoom, @Part("position_offset") String positionOffSet);

    @Multipart
    @POST(VERSION + "/secured/sessions/generateSessionDrawLease.json")
    DrawLeaseToken createSessionDrawLease(@Part("token") String token, @Part("key") String key, @Part("user_id") int userId, @Part("granted") boolean granted);

    @Multipart
    @PUT(VERSION + "/secured/sessions/setPresenterLiveOnSession.json")
    Status sessionSetPresenterLive(@Part("token") String token, @Part("key") String key, @Part("live") boolean live);

    @Multipart
    @PUT(VERSION + "/secured/sessions/setSessionReady.json")
    Status setSessionReady(@Part("token") String token, @Part("key") String key);

    @Multipart
    @POST(VERSION + "/secured/resources/create.json")
    Resource createResource(@Part("token") String token, @Part("resource_data") TypedFile photo, @Part("resource_type") String type, @Part("name") String name, @Part("key") String key, @Part("position") Integer position);

    @Multipart
    @POST(VERSION + "/secured/feedbacks/create.json")
    Status createFeedback(@Part("token") String token, @Part("key") String key, @Part("resource_id") String resourceId, @Part("description") String description);

    @GET(VERSION + "/secured/feedbacks/session.json")
    Feedback.List getFeedbacks(@Query("token") String token, @Query("key") String key);

    @GET(VERSION + "/secured/user.json")
    User getUserProfile(@Query("token") String token);

    @Multipart
    @POST(VERSION + "/secured/notifications/create.json")
    Status createNotification(@Part("token") String token, @Part("key") String key, @Part("notification_type") String notificationType, @Part("content") String content);

    @Multipart
    @POST(VERSION + "/secured/annotations/create.json")
    Annotation createAnnotation(@Part("token") String token, @Part("note") String note, @Part("draw") TypedFile draw, @Part("resource_id") int resourceId, @Part("page") int page, @Part("x_offset") String xOffset, @Part("y_offset") String yOffset, @Part("zoom") String zoom, @Part("draw_token") String drawToken);

    @GET(VERSION + "/secured/annotations/{id}.json")
    Annotation getAnnotation(@Query("token") String token, @Path("id") int annotationId);

}