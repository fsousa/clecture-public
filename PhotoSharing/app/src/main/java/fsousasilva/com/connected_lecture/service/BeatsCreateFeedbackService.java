package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Status;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 1/11/17.
 */

public class BeatsCreateFeedbackService extends RetrofitSpiceRequest<Status, BeatsAPI> {

    private String token;
    private String key;
    private String resourceId;
    private String description;


    public BeatsCreateFeedbackService(String token, String key, String resourceId, String description) {
        super(Status.class, BeatsAPI.class);
        this.token = token;
        this.key = key;
        this.resourceId = resourceId;
        this.description = description;
    }

    @Override
    public Status loadDataFromNetwork() throws Exception {
        return getService().createFeedback(getToken(), getKey(), getResourceId(), getDescription());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
