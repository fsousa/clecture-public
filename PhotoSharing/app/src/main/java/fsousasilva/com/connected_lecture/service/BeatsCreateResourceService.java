package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Resource;
import retrofit.mime.TypedFile;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 12/14/16.
 */

public class BeatsCreateResourceService extends RetrofitSpiceRequest<Resource, BeatsAPI> {

    private String token;
    private TypedFile photo;
    private String type;
    private String name;
    private String key;
    private Integer position;


    public BeatsCreateResourceService(String token, TypedFile photo, String type, String name, String key, Integer position) {
        super(Resource.class, BeatsAPI.class);
        this.token = token;
        this.photo = photo;
        this.name = name.trim();
        this.key = key;
        this.position = position;
        this.type = type;
    }

    @Override
    public Resource loadDataFromNetwork() throws Exception {
        return getService().createResource(getToken(), getPhoto(), getType(), getName(), getKey(), getPosition());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public TypedFile getPhoto() {
        return photo;
    }

    public void setPhoto(TypedFile photo) {
        this.photo = photo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }
}
