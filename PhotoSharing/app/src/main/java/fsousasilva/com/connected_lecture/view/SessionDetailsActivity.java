package fsousasilva.com.connected_lecture.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import fsousasilva.com.connected_lecture.R;
import fsousasilva.com.connected_lecture.controller.AuthController;
import fsousasilva.com.connected_lecture.model.Authenticate;
import fsousasilva.com.connected_lecture.model.Session;
import fsousasilva.com.connected_lecture.model.SessionShort;
import fsousasilva.com.connected_lecture.model.Status;
import fsousasilva.com.connected_lecture.model.Viewer;
import fsousasilva.com.connected_lecture.service.BeatsAddViewToSessionService;
import fsousasilva.com.connected_lecture.service.BeatsDeleteSessionService;
import fsousasilva.com.connected_lecture.service.BeatsGetSessionService;
import fsousasilva.com.connected_lecture.service.RetrofitSpiceService;

public class SessionDetailsActivity extends BaseActivity {

    @BindView(R.id.sessionNameTextView)
    TextView sessionName;
    @BindView(R.id.sessionDetailsDoneButton)
    Button joinButton;
    @BindView(R.id.session_details_delete_button)
    Button deleteButton;
    @BindView(R.id.sessionDetailsViewersGrid)
    GridView viewersGrid;
    @BindView(R.id.presenterNameTextView)
    TextView presenterName;
    @BindView(R.id.sessionDetailsLiveTextView)
    TextView liveTextView;
    @BindView(R.id.empty_list_item)
    TextView emptyTextViewForGridView;

    private SpiceManager mSpiceManager = new SpiceManager(RetrofitSpiceService.class);
    private SessionShort sessionShort;

    @Nullable
    private Session mySession;

    private Authenticate authenticate;
    private Boolean isPresenter = false;
    private ViewerAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_details);

        ButterKnife.bind(this);

        authenticate = AuthController.getInstance().getAuth(getApplicationContext());
        isPresenter = getIntent().getBooleanExtra("isPresenter", false);

        if (!isPresenter) {
            deleteButton.setVisibility(View.GONE);
        }

        String sessionObject = getIntent().getStringExtra("sessionObject");
        Gson gson = new Gson();
        sessionShort = gson.fromJson(sessionObject, SessionShort.class);

        myAdapter = new ViewerAdapter(getApplicationContext());
        viewersGrid.setAdapter(myAdapter);
        viewersGrid.setEmptyView(emptyTextViewForGridView);

        fireGetSessionDetails();

        sessionName.setText(sessionShort.getName());
        presenterName.setText(sessionShort.getPresenter().getName());
        liveTextView.setText(sessionShort.isLive() ? "Yes" : "No");

        joinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != mySession) {

                    if (isPresenter) {
                        goToSessionGaleryAsPresenter(mySession);
                    } else {
                        fireJoinSession(mySession);
                    }
                }
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fireDeleteSessionCall();

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mySession != null) {
            myAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mSpiceManager.isStarted()) {
            mSpiceManager.start(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }
    }

    public void fireJoinSession(Session session) {
        if (mySession != null) {
            showProgressDialog("Joining Lecture");
            BeatsAddViewToSessionService addViewer = new BeatsAddViewToSessionService(authenticate.getToken(), mySession.getKey(), sessionShort.getSecret());
            mSpiceManager.execute(addViewer, "addviewertosession", DurationInMillis.ALWAYS_EXPIRED, new AddViewToSessionRequestListener(session));
        }
    }

    public void fireGetSessionDetails() {
        showProgressDialog("Retrieving Details");
        BeatsGetSessionService getSession = new BeatsGetSessionService(authenticate.getToken(), sessionShort.getKey(), sessionShort.getSecret());
        mSpiceManager.execute(getSession, "getSession", DurationInMillis.ALWAYS_EXPIRED, new SessionDetailsRequestListener());
    }

    public void fireDeleteSessionCall() {
        showProgressDialog("Rolling back changes");
        BeatsDeleteSessionService deleteSession = new BeatsDeleteSessionService(authenticate.getToken(), sessionShort.getKey());
        mSpiceManager.execute(deleteSession, "deleteSession", DurationInMillis.ALWAYS_EXPIRED, new DeleteSessionRequestListener());
    }

    private void goToSessionGalery(Session session) {
        Gson gson = new Gson();
        String sessionString = gson.toJson(session);
        Intent intent = new Intent(SessionDetailsActivity.this, DisplayPhotosViewerActivity.class);
        intent.putExtra("sessionObject", sessionString);
        SessionDetailsActivity.this.startActivity(intent);
        SessionDetailsActivity.this.finish();
    }

    private void goToSessionGaleryAsPresenter(Session session) {
        Gson gson = new Gson();
        String sessionString = gson.toJson(session);
        Intent intent = new Intent(SessionDetailsActivity.this, DisplayPhotosActivity.class);
        intent.putExtra("sessionObject", sessionString);
        SessionDetailsActivity.this.startActivity(intent);
    }

    public final class AddViewToSessionRequestListener implements RequestListener<Viewer> {

        private Session session;

        public AddViewToSessionRequestListener(Session session) {
            this.session = session;
        }

        public Session getSession() {
            return session;
        }

        public void setSession(Session session) {
            this.session = session;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(SessionDetailsActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Viewer viewer) {
            hideProgressDialog();
            if (mySession != null && !mySession.getViewers().contains(viewer)) {
                mySession.getViewers().add(viewer);
            }
            goToSessionGalery(getSession());
        }

    }

    public final class SessionDetailsRequestListener implements RequestListener<Session> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(SessionDetailsActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Session session) {
            hideProgressDialog();
            mySession = session;
            myAdapter.notifyDataSetChanged();
        }

    }

    public final class DeleteSessionRequestListener implements RequestListener<Status> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(SessionDetailsActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Status status) {
            hideProgressDialog();
            SessionDetailsActivity.this.finish();
        }

    }


    private class ViewerAdapter extends BaseAdapter {
        private Context mContext;

        public ViewerAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            if (mySession != null) {
                return mySession.getViewers().size();
            }

            return 0;
        }

        public Object getItem(int position) {
            return mySession.getViewers().get(position);
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {


            TextView nameTextView;
            if (convertView == null) {
                //We must create a View:
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.viewers_row, parent, false);
            }


            Viewer viewer = mySession.getViewers().get(position);
            nameTextView = (TextView) convertView.findViewById(R.id.sessionDetailsViewersRowTextView);
            nameTextView.setText(viewer.getName());

            return convertView;

        }
    }
}
