package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/14/17.
 */

public class DrawLeaseToken {

    @SerializedName("draw_token")
    @Expose
    private String drawToken;

    public DrawLeaseToken(String drawToken) {
        this.drawToken = drawToken;
    }

    public String getDrawToken() {
        return drawToken;
    }

    public void setDrawToken(String drawToken) {
        this.drawToken = drawToken;
    }
}
