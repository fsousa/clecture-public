package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.DrawLeaseToken;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/14/17.
 */

public class BeatsCreateSessionDrawLeaseService extends RetrofitSpiceRequest<DrawLeaseToken, BeatsAPI> {

    private String token;
    private String key;
    private int userId;
    private boolean granted;

    public BeatsCreateSessionDrawLeaseService(String token, String key, int userId, boolean granted) {
        super(DrawLeaseToken.class, BeatsAPI.class);
        this.token = token;
        this.key = key;
        this.userId = userId;
        this.granted = granted;
    }

    @Override
    public DrawLeaseToken loadDataFromNetwork() throws Exception {
        return getService().createSessionDrawLease(getToken(), getKey(), getUserId(), isGranted());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isGranted() {
        return granted;
    }

    public void setGranted(boolean granted) {
        this.granted = granted;
    }
}
