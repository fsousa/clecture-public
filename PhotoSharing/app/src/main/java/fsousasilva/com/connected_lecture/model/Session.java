package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/22/16.
 */

public class Session {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("is_open")
    @Expose
    private Boolean isOpen;
    @SerializedName("presenter")
    @Expose
    private Presenter presenter;
    @SerializedName("viewers")
    @Expose
    private Viewer.List viewers;
    @SerializedName("resources")
    @Expose
    private Resource.List resources;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("current_page")
    @Expose
    private int currentPage;
    @SerializedName("live")
    @Expose
    private boolean live;
    @SerializedName("distance")
    @Expose
    private float distance;

    private String secret;

    public Session(String name, String key, Boolean isOpen, Presenter presenter, Viewer.List viewers, Resource.List resources, String lat, String lng, int currentPage, boolean live, float distance) {
        this.name = name;
        this.key = key;
        this.isOpen = isOpen;
        this.presenter = presenter;
        this.viewers = viewers;
        this.resources = resources;
        this.lat = lat;
        this.lng = lng;
        this.currentPage = currentPage;
        this.live = live;
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Presenter getPresenter() {
        return presenter;
    }

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public Resource.List getResources() {
        return resources;
    }

    public void setResources(Resource.List resources) {
        this.resources = resources;
    }

    public Viewer.List getViewers() {
        return viewers;
    }

    public void setViewers(Viewer.List viewers) {
        this.viewers = viewers;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public static class List extends ArrayList<Session> {

    }
}
