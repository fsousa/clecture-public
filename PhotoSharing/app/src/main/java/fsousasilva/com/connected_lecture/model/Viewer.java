package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/22/16.
 */

public class Viewer {

    @SerializedName("name")
    @Expose
    private String name;

    public Viewer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Viewer && ((Viewer)obj).getName() == this.getName()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    public static class List extends ArrayList<Viewer> {

    }
}
