package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Status;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/10/17.
 */

public class BeatsSessionChangeZoomService extends RetrofitSpiceRequest<Status, BeatsAPI> {
    private String token;
    private String key;
    private String zoom;
    private String positionOffSet;


    public BeatsSessionChangeZoomService(String token, String key, String zoom, String positionOffSet) {
        super(Status.class, BeatsAPI.class);
        this.token = token;
        this.key = key;
        this.zoom = zoom;
        this.positionOffSet = positionOffSet;

    }

    @Override
    public Status loadDataFromNetwork() throws Exception {
        return getService().sessionChangeZoom(getToken(), getKey(), getZoom(), getPositionOffSet());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getZoom() {
        return zoom;
    }

    public void setZoom(String zoom) {
        this.zoom = zoom;
    }

    public String getPositionOffSet() {
        return positionOffSet;
    }

    public void setPositionOffSet(String positionOffSet) {
        this.positionOffSet = positionOffSet;
    }

}
