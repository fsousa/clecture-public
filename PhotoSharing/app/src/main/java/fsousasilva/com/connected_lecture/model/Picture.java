package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 12/14/16.
 */

public class Picture {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("thumb")
    @Expose
    private Thumb thumb;

    public Picture(String url, Thumb thumb) {
        this.url = url;
        this.thumb = thumb;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Thumb getThumb() {
        return thumb;
    }

    public void setThumb(Thumb thumb) {
        this.thumb = thumb;
    }
}
