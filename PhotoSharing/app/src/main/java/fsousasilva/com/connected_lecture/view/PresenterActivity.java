package fsousasilva.com.connected_lecture.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fsousasilva.com.connected_lecture.R;
import fsousasilva.com.connected_lecture.controller.AuthController;
import fsousasilva.com.connected_lecture.model.Authenticate;
import fsousasilva.com.connected_lecture.model.Session;
import fsousasilva.com.connected_lecture.model.SessionShort;
import fsousasilva.com.connected_lecture.service.BeatsCreateSessionService;
import fsousasilva.com.connected_lecture.service.BeatsGetAllPresenterSessionsService;
import fsousasilva.com.connected_lecture.service.RetrofitSpiceService;
import utils.SessionComparator;

public class PresenterActivity extends BaseActivity implements Validator.ValidationListener {

    private final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 10;
    private final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 20;
    @BindView(R.id.sessionNameEditText)
    @NotEmpty
    EditText sessionName;
    @BindView(R.id.sessionSecretEditText)
    @NotEmpty
    EditText sessionSecret;
    @BindView(R.id.createSessionButton)
    Button createSessionButton;
    @BindView(R.id.publicSessionCheckBox)
    CheckBox publicSessionCheckBox;
    @BindView(R.id.presenter_open_session_grid_view)
    GridView openSessionsGridView;
    @BindView(R.id.empty_list_item)
    TextView emptyActiveSessions;

    private Validator validator;
    private SpiceManager mSpiceManager = new SpiceManager(RetrofitSpiceService.class);

    private SessionShort.List mySessions = new SessionShort.List();
    ;
    private SessionAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presenter);

        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);

        checkLocationPermission();
        checkStoragePermission();

        createSessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validator.validate();
            }
        });

        publicSessionCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sessionSecret.setVisibility(View.GONE);
                } else {
                    sessionSecret.setVisibility(View.VISIBLE);
                }

            }
        });

        myAdapter = new SessionAdapter(getApplicationContext());
        openSessionsGridView.setAdapter(myAdapter);
        openSessionsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                goToSessionDetails(mySessions.get(position));
            }
        });
        openSessionsGridView.setEmptyView(emptyActiveSessions);
    }

    private void goToSessionDetails(SessionShort session) {
        Gson gson = new Gson();
        String sessionString = gson.toJson(session);
        Intent intent = new Intent(PresenterActivity.this, SessionDetailsActivity.class);
        intent.putExtra("sessionObject", sessionString);
        intent.putExtra("isPresenter", true);
        PresenterActivity.this.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        fireGetSessionsCall();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mSpiceManager.isStarted()) {
            mSpiceManager.start(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }
    }

    private void fireCreateSessionsCall() {

        Authenticate auth = AuthController.getInstance().getAuth(PresenterActivity.this);
        String name = sessionName.getText().toString();
        String secret = sessionSecret.getText().toString();
        boolean isOpen = publicSessionCheckBox.isChecked();
        Location location = getUserLocation();

        String lat = "0.0";
        String lng = "0.0";
        if (location != null) {
            lat = String.valueOf(location.getLatitude());
            lng = String.valueOf(location.getLongitude());
        }

        showProgressDialog("Creating Lecture");
        BeatsCreateSessionService getSessions = new BeatsCreateSessionService(auth.getToken(), name, lat, lng, isOpen, secret);
        mSpiceManager.execute(getSessions, "createSession", DurationInMillis.ALWAYS_EXPIRED, new CreateSessionRequestListener(secret));

    }

    private void fireGetSessionsCall() {

        showProgressDialog("Loading Lectures");
        Authenticate auth = AuthController.getInstance().getAuth(PresenterActivity.this);
        BeatsGetAllPresenterSessionsService getSessions = new BeatsGetAllPresenterSessionsService(auth.getToken());
        mSpiceManager.execute(getSessions, "sessions", DurationInMillis.ALWAYS_EXPIRED, new SessionRequestListener());

    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(PresenterActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(PresenterActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                Log.i("APP", "We need your permission");

            } else {

                ActivityCompat.requestPermissions(PresenterActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

            }
        }
    }

    private void checkStoragePermission() {
        if (ContextCompat.checkSelfPermission(PresenterActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(PresenterActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                Log.i("APP", "We need your permission");

            } else {

                ActivityCompat.requestPermissions(PresenterActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

            }
        }
    }

    @Nullable
    private Location getUserLocation() {

        if (ContextCompat.checkSelfPermission(PresenterActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            return lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        } else {
            return null;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                break;
            }

            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.i("APP", "Permission granted");

                } else {

                    Log.i("APP", "Create session cancelled. Permission denied");
                }
            }
        }
    }

    @Override
    public void onValidationSucceeded() {
        fireCreateSessionsCall();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public final class CreateSessionRequestListener implements RequestListener<Session> {

        private String secret;

        public CreateSessionRequestListener(String secret) {
            this.secret = secret;
        }

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(PresenterActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Session session) {

            hideProgressDialog();

            Gson gson = new Gson();
            session.setSecret(getSecret());
            String sessionString = gson.toJson(session);
            Intent intent = new Intent(PresenterActivity.this, PhotoGalleryActivity.class);
            intent.putExtra("sessionObject", sessionString);
            PresenterActivity.this.startActivity(intent);
            PresenterActivity.this.finish();
        }

        public String getSecret() {
            return secret;
        }
    }

    public final class SessionRequestListener implements RequestListener<SessionShort.List> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(PresenterActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(SessionShort.List sessions) {
            hideProgressDialog();
            mySessions = sessions;
            Collections.sort(mySessions, new SessionComparator());
            myAdapter.notifyDataSetChanged();
        }

    }

    private class SessionAdapter extends BaseAdapter {
        private Context mContext;

        public SessionAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return mySessions.size();
        }

        public Object getItem(int position) {
            return mySessions.get(position);
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {


            TextView nameTextView;
            TextView presenterTextView;
            ImageView lockerImageView;
            TextView distance;
            if (convertView == null) {
                //We must create a View:
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.session_row, parent, false);
            }

            SessionShort session = mySessions.get(position);
            nameTextView = (TextView) convertView.findViewById(R.id.sessionRowTextView);
            nameTextView.setText(session.getName());

            presenterTextView = (TextView) convertView.findViewById(R.id.sessionRowPresentertextView);
            presenterTextView.setText(String.format("%d Students Connected", session.getNumberOfViewer()));

            lockerImageView = (ImageView) convertView.findViewById(R.id.sessionRowLockimageView);
            distance = (TextView) convertView.findViewById(R.id.sessionRowDistanceTextView);

            if (session.getOpen()) {
                lockerImageView.setImageResource(R.mipmap.unlocked);
            } else {
                lockerImageView.setImageResource(R.mipmap.locked);
            }

            distance.setVisibility(View.GONE);

            return convertView;

        }
    }
}
