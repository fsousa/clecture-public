package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.SessionShort;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/22/16.
 */

public class BeatsGetSessionsService extends RetrofitSpiceRequest<SessionShort.List, BeatsAPI> {

    private String token;
    private String lat;
    private String lng;

    public BeatsGetSessionsService(String token, String lat, String lng) {
        super(SessionShort.List.class, BeatsAPI.class);
        this.token = token;
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    public SessionShort.List loadDataFromNetwork() throws Exception {
        return getService().getSessions(getToken(), getLat(), getLng());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
