package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Status;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/24/16.
 */

public class BeatsCheckSessionSecretService extends RetrofitSpiceRequest<Status, BeatsAPI> {

    private String token;
    private String key;
    private String secret;

    public BeatsCheckSessionSecretService(String token, String key, String secret) {
        super(Status.class, BeatsAPI.class);
        this.token = token;
        this.key = key;
        this.secret = secret.trim();
    }

    @Override
    public Status loadDataFromNetwork() throws Exception {
        return getService().checkSessionSecret(getToken(), getKey(), getSecret());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
