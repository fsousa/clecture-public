package fsousasilva.com.connected_lecture.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.agsw.FabricView.FabricView;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.nineoldandroids.animation.Animator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Timer;
import java.util.TimerTask;

import br.net.bmobile.websocketrails.WebSocketRailsChannel;
import br.net.bmobile.websocketrails.WebSocketRailsDataCallback;
import br.net.bmobile.websocketrails.WebSocketRailsDispatcher;
import butterknife.BindView;
import butterknife.ButterKnife;
import fsousasilva.com.connected_lecture.R;
import fsousasilva.com.connected_lecture.controller.AuthController;
import fsousasilva.com.connected_lecture.controller.ConfigurationController;
import fsousasilva.com.connected_lecture.controller.UserController;
import fsousasilva.com.connected_lecture.model.Authenticate;
import fsousasilva.com.connected_lecture.model.DrawNotification;
import fsousasilva.com.connected_lecture.model.Feedback;
import fsousasilva.com.connected_lecture.model.Media;
import fsousasilva.com.connected_lecture.model.NotificationTypeEnum;
import fsousasilva.com.connected_lecture.model.PDFDraw;
import fsousasilva.com.connected_lecture.model.RemoteNotification;
import fsousasilva.com.connected_lecture.model.Resource;
import fsousasilva.com.connected_lecture.model.Session;
import fsousasilva.com.connected_lecture.model.Status;
import fsousasilva.com.connected_lecture.service.BeatsCreateAnnotationService;
import fsousasilva.com.connected_lecture.service.BeatsCreateSessionDrawLeaseService;
import fsousasilva.com.connected_lecture.service.BeatsGetFeedbacksService;
import fsousasilva.com.connected_lecture.service.BeatsSessionChangePageService;
import fsousasilva.com.connected_lecture.service.BeatsSessionChangeZoomService;
import fsousasilva.com.connected_lecture.service.BeatsSetPresenterLiveOnSessionService;
import fsousasilva.com.connected_lecture.service.RetrofitSpiceService;
import retrofit.mime.TypedFile;
import utils.DownloadFileUtil;
import utils.FileUtil;
import utils.ResourceComparator;

public class DisplayPhotosActivity extends BaseActivity implements OnDrawListener {


    @BindView(R.id.presenter_pdf_view)
    PDFView presenterSlider;
    @BindView(R.id.presenter_display_raise_hand_imageview)
    ImageView raiseHandImageView;
    @BindView(R.id.faricView)
    FabricView drawCanvas;
    @BindView(R.id.presenter_view_draw_color_menu)
    FloatingActionMenu floatingColorMenu;
    @BindView(R.id.presenter_view_draw_color_red_button)
    FloatingActionButton colorRedFloatingButton;
    @BindView(R.id.presenter_view_draw_color_green_button)
    FloatingActionButton colorGreenFloatingButton;
    @BindView(R.id.presenter_view_draw_color_blue_button)
    FloatingActionButton colorBlueFloatingButton;
    @BindView(R.id.presenter_view_draw_color_black_button)
    FloatingActionButton colorBlackFloatingButton;
    @BindView(R.id.presenter_view_draw_clean_button)
    FloatingActionButton cleanDrawButton;

    private Session session;
    private SpiceManager mSpiceManager = new SpiceManager(RetrofitSpiceService.class);
    private WebSocketRailsDispatcher dispatcher;
    private WebSocketRailsChannel webSocketRailsChannelNotification;
    private WebSocketRailsChannel webSocketRailsDrawChannel;
    private Boolean isDrawing = false;
    private Paint mPaint;
    private SparseArray<PDFDraw> drawingMap = new SparseArray<>();

    private Timer sessionTimerSync;
    private TimerTask updateSessionTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_photos);

        ButterKnife.bind(this);

        Bundle bundle = this.getIntent().getExtras();
        ArrayList<String> images = bundle.getStringArrayList("photosList");

        String sessionObject = getIntent().getStringExtra("sessionObject");
        Gson gson = new Gson();
        session = gson.fromJson(sessionObject, Session.class);

        subscribeToWebsocket();

        LinkedHashMap<String, Object> url_maps = new LinkedHashMap<>();
        if (images == null) {
            Collections.sort(session.getResources(), new ResourceComparator());
            for (Resource imgs : session.getResources()) {
                url_maps.put(imgs.getName(), imgs.getPicture().getUrl());
            }
        } else {
            for (String imgs : images) {
                url_maps.put(imgs, new File(imgs));
            }
        }


        PDFView.Configurator conf;
        for (String name : url_maps.keySet()) {

            Object item = url_maps.get(name);
            if (item instanceof File) {
                conf = presenterSlider.fromFile((File) item);
                finishPdfConfiguration(conf);
            } else {
                downloadFiles((String) item);
            }

            //TODO
            //By now we only add one resource
            break;

        }

        raiseHandImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                raiseHandImageView.setVisibility(View.GONE);
            }
        });

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

    }

    private void setupFloatingColorButtons() {
        colorBlackFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawCanvas.setColor(Color.BLACK);
                floatingColorMenu.close(true);
            }
        });

        colorRedFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawCanvas.setColor(Color.RED);
                floatingColorMenu.close(true);
            }
        });

        colorGreenFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawCanvas.setColor(Color.GREEN);
                floatingColorMenu.close(true);
            }
        });

        colorBlueFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawCanvas.setColor(Color.BLUE);
                floatingColorMenu.close(true);
            }
        });

        cleanDrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawCanvas.cleanPage();
            }
        });
    }

    private void unsubscribeToWebSocket() {
        if (dispatcher != null) {
            dispatcher.disconnect();
        }
    }

    private void subscribeToWebsocket() {

        try {
            dispatcher = new WebSocketRailsDispatcher(new URL(ConfigurationController.getInstance().getServerFull() + "/websocket"));
            dispatcher.connect();

            webSocketRailsDrawChannel = dispatcher.subscribe("draw");
            webSocketRailsDrawChannel.bind(session.getKey(), new WebSocketRailsDataCallback() {
                @Override
                public void onDataAvailable(Object data) {
                    Gson gson = new Gson();
                    final DrawNotification drawNotification = gson.fromJson(data.toString(), DrawNotification.class);

                    ImageLoader imageLoader = ImageLoader.getInstance();
                    imageLoader.loadImage(drawNotification.getUrl(), new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            Bitmap bitmap = Bitmap.createScaledBitmap(loadedImage, presenterSlider.getWidth(), presenterSlider.getHeight(), false);
                            PDFDraw item = new PDFDraw(bitmap.getHeight(), bitmap.getWidth(), bitmap, presenterSlider.getCurrentXOffset(), presenterSlider.getCurrentYOffset(), drawNotification.getZoom());
                            drawingMap.append(presenterSlider.getCurrentPage(), item);

                            Handler uiHandler = new Handler(Looper.getMainLooper());
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    presenterSlider.invalidate();
                                }
                            };
                            uiHandler.post(runnable);
                        }
                    });
                }
            });

            webSocketRailsChannelNotification = dispatcher.subscribe("notification");
            webSocketRailsChannelNotification.bind(UserController.getInstance().getUser(getApplicationContext()).getUsername(), new WebSocketRailsDataCallback() {

                @Override
                public void onDataAvailable(final Object data) {
                    Gson gson = new Gson();
                    final RemoteNotification remoteNotification = gson.fromJson(data.toString(), RemoteNotification.class);

                    Handler uiHandler = new Handler(Looper.getMainLooper());
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (remoteNotification.getNotificationType().equals(NotificationTypeEnum.RAISE_HAND.toString())) {
                                handleRaiseHandEvent();
                            } else if (remoteNotification.getNotificationType().equals(NotificationTypeEnum.GOTO.toString())) {
                                handleGotoEvent(Integer.valueOf(remoteNotification.getNotificationContent()));
                            } else if (remoteNotification.getNotificationType().equals(NotificationTypeEnum.ASK_PERMISSION_TO_DRAW.toString())) {
                                handleAskToDrawEvent(Integer.valueOf(remoteNotification.getNotificationContent()));
                            }
                        }
                    };
                    uiHandler.post(runnable);
                }
            });
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void handleAskToDrawEvent(final int userId) {
        new MaterialDialog.Builder(DisplayPhotosActivity.this)
                .title("Ask to Draw")
                .content("A student wants to draw on the screen. Do you authorize ?")
                .negativeText("No")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        fireCreateDrawLeaseCall(userId, false);
                    }
                })
                .positiveText("Yes")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        fireCreateDrawLeaseCall(userId, true);
                    }
                })
                .show();

    }

    private void handleGotoEvent(final int page) {
        new MaterialDialog.Builder(DisplayPhotosActivity.this)
                .title("Go To Page")
                .content("A Student wats to change the current page. Do you authorize ?")
                .negativeText("No")
                .positiveText("Yes")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        presenterSlider.jumpTo(page);
                        fireChangePageCall(session, page);
                    }
                })
                .show();
    }

    private void handleRaiseHandEvent() {
        YoYo.with(Techniques.SlideInUp).withListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                raiseHandImageView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                raiseHandImageView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        raiseHandImageView.setVisibility(View.GONE);
                    }
                }, 2000);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).duration(1000).playOn(raiseHandImageView);
    }

    private void downloadFiles(String url) {
        showProgressDialog("Downloading Files");

        DownloadFileUtil downloadFileUtil = new DownloadFileUtil(this, new DownloadStatusListenerV1() {
            @Override
            public void onDownloadComplete(DownloadRequest downloadRequest) {
                hideProgressDialog();
                finishPdfConfiguration(presenterSlider.fromFile(new File(downloadRequest.getDestinationURI().getPath())));

            }

            @Override
            public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {
                Toast.makeText(DisplayPhotosActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                hideProgressDialog();

            }

            @Override
            public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {

            }
        });

        downloadFileUtil.downloadFiles(url);
    }

    private void finishPdfConfiguration(PDFView.Configurator conf) {
        assert conf != null;
        conf.defaultPage(0).enableSwipe(!isDrawing).swipeHorizontal(true).onPageChange(new OnPageChangeListener() {
            @Override
            public void onPageChanged(int page, int pageCount) {
                fireChangePageCall(session, page);
            }
        }).onDraw(this).load();

        setupFloatingColorButtons();

        updateSessionTask = new TimerTask() {
            @Override
            public void run() {
                pdfViewWatchdog();
            }
        };

        if (sessionTimerSync == null) {
            sessionTimerSync = new Timer();
            sessionTimerSync.scheduleAtFixedRate(updateSessionTask, 0, 5000);
        }


    }

    private Paint getPaintAreaForBitmap() {
        if (mPaint == null) {
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setFilterBitmap(true);
            mPaint.setDither(true);
        }

        return mPaint;
    }

    private int getCurrentResourceId() {
        //TODO
        //Only handle one resource
        return Integer.valueOf(session.getResources().get(0).getId());
    }

    public void fireCreateDrawLeaseCall(int userId, boolean granted) {
        Authenticate authenticate = AuthController.getInstance().getAuth(getApplicationContext());
        BeatsCreateSessionDrawLeaseService drawLease = new BeatsCreateSessionDrawLeaseService(authenticate.getToken(), session.getKey(), userId, granted);
        mSpiceManager.execute(drawLease, "userDrawLease", DurationInMillis.ALWAYS_EXPIRED, null);
    }

    public void fireZoomChangedCall(float zoom, float positionOffSet) {
        Authenticate authenticate = AuthController.getInstance().getAuth(getApplicationContext());
        BeatsSessionChangeZoomService nextpage = new BeatsSessionChangeZoomService(authenticate.getToken(), session.getKey(), String.valueOf(zoom), String.valueOf(positionOffSet));
        mSpiceManager.execute(nextpage, "changezoom", DurationInMillis.ALWAYS_EXPIRED, null);
    }

    public void fireChangePageCall(Session session, int currentPage) {
        Authenticate authenticate = AuthController.getInstance().getAuth(getApplicationContext());
        BeatsSessionChangePageService nextpage = new BeatsSessionChangePageService(authenticate.getToken(), session.getKey(), currentPage);
        mSpiceManager.execute(nextpage, "changepage", DurationInMillis.ALWAYS_EXPIRED, new ChangePageRequestListener());
    }

    public void fireSetSessionLive(boolean live) {
        Authenticate authenticate = AuthController.getInstance().getAuth(getApplicationContext());
        BeatsSetPresenterLiveOnSessionService sessionLive = new BeatsSetPresenterLiveOnSessionService(authenticate.getToken(), session.getKey(), live);
        mSpiceManager.execute(sessionLive, "sessionLive", DurationInMillis.ALWAYS_EXPIRED, new SessionLiveRequestListener());
    }

    public void fireGetFeedbacks() {
        showProgressDialog("Downloading Feedbacks");
        Authenticate auth = AuthController.getInstance().getAuth(DisplayPhotosActivity.this);
        BeatsGetFeedbacksService getSessions = new BeatsGetFeedbacksService(auth.getToken(), session.getKey());
        mSpiceManager.execute(getSessions, "fireGetFeedbacks", DurationInMillis.ALWAYS_EXPIRED, new ViewFeedbackRequestListener());
    }

    public void fireCreateAnnotation(String note, File bitmap, int resourceId, int page, String xOffset, String yOffset, String zoom) {

        TypedFile draw = new TypedFile(Media.MediaType.PNG.name(), bitmap);
        Authenticate auth = AuthController.getInstance().getAuth(DisplayPhotosActivity.this);
        BeatsCreateAnnotationService createAnnotationService = new BeatsCreateAnnotationService(auth.getToken(), note, draw, resourceId, page, xOffset, yOffset, zoom);
        mSpiceManager.execute(createAnnotationService, "fireCreateAnnotation", DurationInMillis.ALWAYS_EXPIRED, null);
    }

    @Override
    public void onLayerDrawn(Canvas canvas, float pageWidth, float pageHeight, int displayedPage) {

        pdfViewWatchdog();

        PDFDraw draw = drawingMap.get(displayedPage);
        if (draw != null) {

            Paint paint = getPaintAreaForBitmap();
            float currentZoom = presenterSlider.getZoom();

            if (currentZoom != draw.getZoom()) {

                float zoom = currentZoom / draw.getZoom();

                int wantedW = (int) (zoom * draw.getWidth());
                int wantedH = (int) (zoom * draw.getHeight());

                float offSetX = zoom * draw.getOffSetX();
                float offSetY = zoom * draw.getOffSetY();

                Bitmap bitmap = Bitmap.createScaledBitmap(draw.getImage(), wantedW, wantedH, false);
                canvas.drawBitmap(bitmap, -offSetX / pageWidth, -offSetY, paint);
                bitmap.recycle();

            } else {

                float offSetX = draw.getOffSetX();
                float offSetY = draw.getOffSetY();

                canvas.drawBitmap(draw.getImage(), -offSetX / pageWidth, -offSetY, paint);
            }
        }
    }

    private void pdfViewWatchdog() {

        float currentZoom = presenterSlider.getZoom();
        float positionOffSet = presenterSlider.getPositionOffset();

        fireZoomChangedCall(currentZoom, positionOffSet);

    }


    @Override
    public void onStart() {
        super.onStart();
        if (!mSpiceManager.isStarted()) {
            mSpiceManager.start(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        fireSetSessionLive(true);

    }

    @Override
    protected void onPause() {
        fireSetSessionLive(false);
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }

        unsubscribeToWebSocket();


        if (sessionTimerSync != null) {
            sessionTimerSync.cancel();
            sessionTimerSync = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_presenter_display, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_view_feedback) {
            fireGetFeedbacks();
            return true;
        } else if (id == R.id.action_draw) {
            if (isDrawing) {
                stopDrawing(item);
            } else {
                startDrawing(item);
            }

            isDrawing = !isDrawing;
        }

        return super.onOptionsItemSelected(item);
    }

    private void stopDrawing(MenuItem item) {
        item.setIcon(getResources().getDrawable(R.mipmap.pencil_white));

        floatingColorMenu.setVisibility(View.GONE);
        cleanDrawButton.setVisibility(View.GONE);

        presenterSlider.enableSwipe(true);

        Bitmap bitmap = drawCanvas.getCanvasBitmap();
        int page = presenterSlider.getCurrentPage();
        float xOffset = presenterSlider.getCurrentXOffset();
        float yOffset = presenterSlider.getCurrentYOffset();
        float zoom = presenterSlider.getZoom();

        Bitmap emptyBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        if (!emptyBitmap.sameAs(bitmap)) {
            emptyBitmap.recycle();

            drawingMap.append(page, new PDFDraw(drawCanvas.getHeight(), drawCanvas.getWidth(), bitmap, xOffset, yOffset, zoom));

            FileUtil fileUtil = new FileUtil(DisplayPhotosActivity.this);
            File bitmapFile = fileUtil.saveBitmapToTempFile("tempbitmap" + page + ".png", bitmap);

            if (bitmapFile != null) {
                fireCreateAnnotation("", bitmapFile, getCurrentResourceId(), page, String.valueOf(xOffset), String.valueOf(yOffset), String.valueOf(zoom));
            }


        }

        drawCanvas.cleanPage();
        drawCanvas.setVisibility(View.GONE);
        presenterSlider.invalidate();

    }

    private void startDrawing(MenuItem item) {
        item.setIcon(getResources().getDrawable(R.mipmap.pencil_green));

        drawingMap.remove(presenterSlider.getCurrentPage());

        cleanDrawButton.setVisibility(View.VISIBLE);
        floatingColorMenu.setVisibility(View.VISIBLE);


        presenterSlider.enableSwipe(false);
        drawCanvas.cleanPage();
        presenterSlider.invalidate();

        drawCanvas.setVisibility(View.VISIBLE);
        drawCanvas.setColor(Color.RED);
        drawCanvas.setBackgroundColor(Color.TRANSPARENT);

    }

    public final class ChangePageRequestListener implements RequestListener<Status> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Toast.makeText(DisplayPhotosActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Status status) {
            //Do nothing
        }

    }

    public final class SessionLiveRequestListener implements RequestListener<Status> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Toast.makeText(DisplayPhotosActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Status status) {

            //Do nothing.

        }
    }

    public final class ViewFeedbackRequestListener implements RequestListener<Feedback.List> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(DisplayPhotosActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Feedback.List feedbacks) {
            hideProgressDialog();

            if (feedbacks.size() != 0) {
                new MaterialDialog.Builder(DisplayPhotosActivity.this)
                        .title("Comments List")
                        .adapter(new FeedbackAdapter(feedbacks), null)
                        .positiveText("Close")
                        .show();
            } else {
                new MaterialDialog.Builder(DisplayPhotosActivity.this)
                        .title("No Comments")
                        .content("This document has no comments yet")
                        .positiveText("Ok")
                        .show();
            }
        }
    }

    private class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {
        private Feedback.List myDataset;

        public FeedbackAdapter(Feedback.List myDataset) {
            this.myDataset = myDataset;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.description.setText(myDataset.get(position).getDescription());
            holder.viewerName.setText(myDataset.get(position).getViewer().getName());

        }

        @Override
        public int getItemCount() {
            return myDataset.size();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.feedbacks_row, parent, false);

            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            // each data item is just a string in this case
            TextView description;
            TextView viewerName;

            public ViewHolder(View v) {
                super(v);
                description = (TextView) v.findViewById(R.id.feedbacksDescriptionRowTextView);
                viewerName = (TextView) v.findViewById(R.id.feedbacksViewerRowTextView);
            }
        }
    }

}
