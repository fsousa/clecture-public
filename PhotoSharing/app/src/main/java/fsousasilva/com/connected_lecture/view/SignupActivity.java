package fsousasilva.com.connected_lecture.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.exception.NetworkException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.net.HttpURLConnection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fsousasilva.com.connected_lecture.R;
import fsousasilva.com.connected_lecture.controller.AuthController;
import fsousasilva.com.connected_lecture.controller.UserController;
import fsousasilva.com.connected_lecture.model.Authenticate;
import fsousasilva.com.connected_lecture.model.User;
import fsousasilva.com.connected_lecture.service.BeatsCreateAccountService;
import fsousasilva.com.connected_lecture.service.BeatsGetUserProfileService;
import fsousasilva.com.connected_lecture.service.RetrofitSpiceService;
import retrofit.RetrofitError;

public class SignupActivity extends BaseActivity implements Validator.ValidationListener {

    @NotEmpty
    @BindView(R.id.signupUsernameeditText)
    EditText username;
    @Password(min = 6, scheme = Password.Scheme.ALPHA_NUMERIC, message = "Min 6 digits. Letters and numbers.")
    @BindView(R.id.signupPasswordEditText)
    EditText password;
    @ConfirmPassword
    @BindView(R.id.signupRepeatPasswordEditText)
    EditText repeatPassword;
    @Email
    @BindView(R.id.signupEmailEditText)
    EditText email;
    @NotEmpty
    @BindView(R.id.signupFirstNameEditText)
    EditText firstName;
    @NotEmpty
    @BindView(R.id.signupLastNameEditText)
    EditText lastName;
    @BindView(R.id.signupDoneButton)
    Button doneButton;

    private Validator validator;
    private SpiceManager mSpiceManager = new SpiceManager(RetrofitSpiceService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        validator = new Validator(this);
        validator.setValidationListener(this);

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mSpiceManager.isStarted()) {
            mSpiceManager.start(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }
    }

    @Override
    public void onValidationSucceeded() {
        fireSignupCall();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void fireGetUserProfileCall() {
        showProgressDialog();
        Authenticate auth = AuthController.getInstance().getAuth(SignupActivity.this);
        BeatsGetUserProfileService createAccount = new BeatsGetUserProfileService(auth.getToken());
        mSpiceManager.execute(createAccount, "getprofile", DurationInMillis.ALWAYS_EXPIRED, new UserProfileRequestListener());
    }

    private void fireSignupCall() {

        showProgressDialog();
        BeatsCreateAccountService createAccount = new BeatsCreateAccountService(username.getText().toString(), password.getText().toString(), firstName.getText().toString(), lastName.getText().toString(), email.getText().toString());
        mSpiceManager.execute(createAccount, "signup", DurationInMillis.ALWAYS_EXPIRED, new SignupRequestListener());

    }

    public final class UserProfileRequestListener implements RequestListener<User> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Toast.makeText(SignupActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(User user) {

            UserController.getInstance().saveUser(SignupActivity.this, user);

            Intent intent = new Intent(SignupActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SignupActivity.this.startActivity(intent);
        }
    }

    public final class SignupRequestListener implements RequestListener<Authenticate> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            if (spiceException instanceof NetworkException) {
                NetworkException exception = (NetworkException) spiceException;
                if (exception.getCause() instanceof RetrofitError) {
                    RetrofitError error = (RetrofitError) exception.getCause();
                    int httpErrorCode = error.getResponse().getStatus();
                    if (httpErrorCode == HttpURLConnection.HTTP_CONFLICT) {
                        username.setError("Username already exist");
                    }
                }
            } else {
                Toast.makeText(SignupActivity.this, "failure", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onRequestSuccess(Authenticate authenticate) {

            hideProgressDialog();
            AuthController.getInstance().saveAuth(SignupActivity.this, authenticate);

            fireGetUserProfileCall();

        }

    }
}
