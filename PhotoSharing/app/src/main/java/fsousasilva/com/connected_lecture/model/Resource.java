package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 12/14/16.
 */

public class Resource {

    @SerializedName("resource_type")
    @Expose
    String type;
    @SerializedName("annotations")
    @Expose
    Annotation.List annotations;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("position")
    @Expose
    private Integer position;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("data")
    @Expose
    private Picture picture;

    public Resource(String id, Integer position, String name, Picture picture, String type, Annotation.List annotations) {
        this.id = id;
        this.position = position;
        this.name = name;
        this.picture = picture;
        this.type = type;
        this.annotations = annotations;
    }

    public static int compare(Resource o1, Resource o2) {
        return o1.getPosition().compareTo(o2.getPosition());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static class List extends ArrayList<Resource> {

    }
}
