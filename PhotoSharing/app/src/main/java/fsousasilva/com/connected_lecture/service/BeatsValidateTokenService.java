package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Status;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/23/16.
 */

public class BeatsValidateTokenService extends RetrofitSpiceRequest<Status, BeatsAPI> {

    private String token;

    public BeatsValidateTokenService(String token) {
        super(Status.class, BeatsAPI.class);
        this.token = token;
    }

    @Override
    public Status loadDataFromNetwork() throws Exception {
        return getService().validate(getToken());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
