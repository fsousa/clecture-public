package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Authenticate;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 11/22/16.
 */

public class BeatsAuthService extends RetrofitSpiceRequest<Authenticate, BeatsAPI> {

    private String userName;
    private String password;

    public BeatsAuthService(String userName, String password) {
        super(Authenticate.class, BeatsAPI.class);
        this.userName = userName;
        this.password = password;
    }

    @Override
    public Authenticate  loadDataFromNetwork() throws Exception {
        return getService().auth(getUserName(), getPassword());
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}