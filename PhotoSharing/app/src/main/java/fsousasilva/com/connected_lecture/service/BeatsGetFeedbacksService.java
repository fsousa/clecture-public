package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Feedback;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 1/11/17.
 */

public class BeatsGetFeedbacksService extends RetrofitSpiceRequest<Feedback.List, BeatsAPI> {

    private String token;
    private String key;

    public BeatsGetFeedbacksService(String token, String key) {
        super(Feedback.List.class, BeatsAPI.class);
        this.token = token;
        this.key = key;
    }

    @Override
    public Feedback.List loadDataFromNetwork() throws Exception {
        return getService().getFeedbacks(getToken(), getKey());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
