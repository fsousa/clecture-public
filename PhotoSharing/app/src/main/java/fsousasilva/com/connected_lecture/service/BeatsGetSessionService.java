package fsousasilva.com.connected_lecture.service;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 12/14/16.
 */

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Session;

public class BeatsGetSessionService extends RetrofitSpiceRequest<Session, BeatsAPI> {

    private String token;
    private String key;
    private String secret;

    public BeatsGetSessionService(String token, String key, String secret) {
        super(Session.class, BeatsAPI.class);
        this.token = token;
        this.key = key;
        this.secret = secret;
    }

    @Override
    public Session loadDataFromNetwork() throws Exception {
        return getService().getSession(getToken(), getKey(), getSecret());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}