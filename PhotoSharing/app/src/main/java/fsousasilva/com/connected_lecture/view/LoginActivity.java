package fsousasilva.com.connected_lecture.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.exception.NetworkException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import io.fabric.sdk.android.Fabric;
import java.net.HttpURLConnection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fsousasilva.com.connected_lecture.R;
import fsousasilva.com.connected_lecture.controller.AuthController;
import fsousasilva.com.connected_lecture.controller.UserController;
import fsousasilva.com.connected_lecture.model.Authenticate;
import fsousasilva.com.connected_lecture.model.Status;
import fsousasilva.com.connected_lecture.model.User;
import fsousasilva.com.connected_lecture.service.BeatsAuthService;
import fsousasilva.com.connected_lecture.service.BeatsGetUserProfileService;
import fsousasilva.com.connected_lecture.service.BeatsValidateTokenService;
import fsousasilva.com.connected_lecture.service.RetrofitSpiceService;
import retrofit.RetrofitError;

public class LoginActivity extends BaseActivity implements Validator.ValidationListener {

    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.registerButton)
    Button registerButton;
    @NotEmpty
    @BindView(R.id.usernameLoginEditText)
    EditText username;
    @NotEmpty
    @BindView(R.id.passwordLoginEditText)
    EditText password;

    private Validator validator;
    private SpiceManager mSpiceManager = new SpiceManager(RetrofitSpiceService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                LoginActivity.this.startActivity(intent);
            }
        });

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName("App Open"));



    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mSpiceManager.isStarted()) {
            mSpiceManager.start(this);
        }

        fireValidateTokenCall();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onValidationSucceeded() {
        fireLoginCall();
    }

    private void goToMainScreen() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        LoginActivity.this.startActivity(intent);
    }

    private void fireLoginCall() {
        showProgressDialog("Logging in");
        BeatsAuthService authRequest = new BeatsAuthService(username.getText().toString(), password.getText().toString());
        mSpiceManager.execute(authRequest, "auth", DurationInMillis.ALWAYS_EXPIRED, new AuthRequestListener());
    }

    private void fireValidateTokenCall() {
        Authenticate auth = AuthController.getInstance().getAuth(LoginActivity.this);

        if (auth != null) {
            showProgressDialog();
            BeatsValidateTokenService validateTokenRequest = new BeatsValidateTokenService(auth.getToken());
            mSpiceManager.execute(validateTokenRequest, "validate", DurationInMillis.ALWAYS_EXPIRED, new ValidateTokenRequestListener());
        }
    }

    public void fireGetUserProfile() {
        showProgressDialog();
        Authenticate auth = AuthController.getInstance().getAuth(LoginActivity.this);
        BeatsGetUserProfileService getSessions = new BeatsGetUserProfileService(auth.getToken());
        mSpiceManager.execute(getSessions, "userprofile", DurationInMillis.ALWAYS_EXPIRED, new UserProfileRequestListener());
    }

    public final class UserProfileRequestListener implements RequestListener<User> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            Toast.makeText(LoginActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(User user) {
            hideProgressDialog();
            UserController.getInstance().saveUser(LoginActivity.this, user);
            goToMainScreen();

        }

    }

    public final class AuthRequestListener implements RequestListener<Authenticate> {


        @Override
        public void onRequestFailure(SpiceException spiceException) {
            hideProgressDialog();
            if (spiceException instanceof NetworkException) {
                NetworkException exception = (NetworkException) spiceException;
                if (exception.getCause() instanceof RetrofitError) {
                    RetrofitError error = (RetrofitError) exception.getCause();
                    int httpErrorCode = error.getResponse().getStatus();
                    if (httpErrorCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                Toast.makeText(LoginActivity.this, "failure", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onRequestSuccess(Authenticate authenticate) {
            hideProgressDialog();
            AuthController.getInstance().saveAuth(LoginActivity.this, authenticate);
            fireGetUserProfile();
        }

    }

    private class ValidateTokenRequestListener implements RequestListener<Status> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            // Do nothing
            hideProgressDialog();
        }

        @Override
        public void onRequestSuccess(Status status) {
            hideProgressDialog();
            fireGetUserProfile();
        }
    }
}
