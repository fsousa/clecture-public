package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.Status;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 12/14/16.
 */

public class BeatsSessionChangePageService extends RetrofitSpiceRequest<Status, BeatsAPI> {
    private String token;
    private String key;
    private int currentPage;

    public BeatsSessionChangePageService(String token, String key, int currentPage) {
        super(Status.class, BeatsAPI.class);
        this.token = token;
        this.key = key;
        this.currentPage = currentPage;
    }

    @Override
    public Status loadDataFromNetwork() throws Exception {
        return getService().sessionChangePage(getToken(), getKey(), getCurrentPage());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }
}
