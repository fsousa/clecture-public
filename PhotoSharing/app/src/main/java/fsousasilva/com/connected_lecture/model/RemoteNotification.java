package fsousasilva.com.connected_lecture.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/5/17.
 */

public class RemoteNotification {

    @SerializedName("notification_content")
    private String notificationContent;
    @SerializedName("notification_type")
    private String notificationType;

    public RemoteNotification(String notificationContent, String notificationType) {
        this.notificationContent = notificationContent;
        this.notificationType = notificationType;
    }

    public String getNotificationContent() {
        return notificationContent;
    }

    public void setNotificationContent(String notificationContent) {
        this.notificationContent = notificationContent;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }
}
