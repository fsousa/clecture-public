package fsousasilva.com.connected_lecture.service;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import fsousasilva.com.connected_lecture.model.NotificationTypeEnum;
import fsousasilva.com.connected_lecture.model.Status;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/5/17.
 */

public class BeatsCreateNotificationService extends RetrofitSpiceRequest<Status, BeatsAPI> {

    private String token;
    private String key;
    private NotificationTypeEnum notificationType;
    private String content;

    public BeatsCreateNotificationService(String token, String key, NotificationTypeEnum notificationType, String content) {
        super(Status.class, BeatsAPI.class);
        this.token = token;
        this.key = key;
        this.notificationType = notificationType;
        this.content = content;
    }

    @Override
    public Status loadDataFromNetwork() throws Exception {
        return getService().createNotification(getToken(), getKey(), getNotificationType().toString(), getContent());
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public NotificationTypeEnum getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationTypeEnum notificationType) {
        this.notificationType = notificationType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
