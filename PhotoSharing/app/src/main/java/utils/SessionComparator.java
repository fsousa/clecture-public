package utils;

import java.util.Comparator;

import fsousasilva.com.connected_lecture.model.SessionShort;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 1/19/17.
 */

public class SessionComparator implements Comparator<SessionShort> {

    @Override
    public int compare(SessionShort o1, SessionShort o2) {
        return Float.compare(o1.getDistance(), o2.getDistance());
    }
}
