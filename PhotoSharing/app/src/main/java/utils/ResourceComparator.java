package utils;

import java.util.Comparator;

import fsousasilva.com.connected_lecture.model.Resource;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 1/24/17.
 */

public class ResourceComparator implements Comparator<Resource> {
    @Override
    public int compare(Resource o1, Resource o2) {
        return o1.getPosition().compareTo(o2.getPosition());
    }
}
