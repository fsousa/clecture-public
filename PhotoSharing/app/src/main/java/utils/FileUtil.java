package utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/13/17.
 */

public class FileUtil {

    private Activity activity;

    public FileUtil(Activity activity) {
        this.activity = activity;
    }

    public File getTempFile(String url) {

        File file = null;
        try {
            String fileName = Uri.parse(url).getLastPathSegment();
            file = File.createTempFile(fileName, null, activity.getApplicationContext().getCacheDir());
            file.deleteOnExit();
        } catch (IOException e) {
            // Error while creating file
        }
        return file;
    }

    public File getExternalStorageFile(String url) {
        File f3=new File(Environment.getExternalStorageDirectory()+"/cLecture/");
        if(!f3.exists()) {
            f3.mkdirs();
        }

        return new File(Environment.getExternalStorageDirectory() + "/cLecture/" + url);


    }

    @Nullable
    public File saveBitmapToTempFile(String name, Bitmap bitmap) {

        File file = null;
        try {
            file = getExternalStorageFile(name);
            OutputStream outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 85, outStream);
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return file;
    }
}
