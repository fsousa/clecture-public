package utils;

import android.app.Activity;
import android.net.Uri;

import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.ThinDownloadManager;

import java.io.File;
import java.io.IOException;

/**
 * Created by Felipe Sousa - felipe@quimbik.com on 2/13/17.
 */

public class DownloadFileUtil {

    private DownloadStatusListenerV1 callback;
    private FileUtil fileUtil;

    public DownloadFileUtil(Activity activity, DownloadStatusListenerV1 callback) {
        this.callback = callback;
        this.fileUtil = new FileUtil(activity);
    }

    public void downloadFiles(String url) {

        Uri downloadUri = Uri.parse(url);
        Uri destinationUri = Uri.fromFile(fileUtil.getTempFile(url));
        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.NORMAL)
                .setStatusListener(new DownloadStatusListenerV1() {
                    @Override
                    public void onDownloadComplete(DownloadRequest downloadRequest) {
                        callback.onDownloadComplete(downloadRequest);
                    }

                    @Override
                    public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {
                        callback.onDownloadFailed(downloadRequest, errorCode, errorMessage);

                    }

                    @Override
                    public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {
                        callback.onProgress(downloadRequest, totalBytes, downloadedBytes, progress);
                    }
                });


        ThinDownloadManager downloadManager = new ThinDownloadManager();
        downloadManager.add(downloadRequest);
    }

}
